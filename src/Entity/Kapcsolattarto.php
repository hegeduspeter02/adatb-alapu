<?php

namespace App\Entity;

use App\Repository\KapcsolattartoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KapcsolattartoRepository::class)]
class Kapcsolattarto
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $felhasznalo_id = null;

    #[ORM\Column(length: 50)]
    private ?string $nev = null;

    #[ORM\Column(length: 255)]
    private ?string $jelszo = null;

    #[ORM\Column(length: 50)]
    private ?string $email = null;

    #[ORM\Column(type: "json", nullable: false)]
    private array $jogosultsag = [];

    #[ORM\Column(length: 30)]
    private ?string $beosztas = null;

    public function getFelhasznaloId(): ?int
    {
        return $this->felhasznalo_id;
    }

    public function setFelhasznaloId(?int $felhasznalo_id): Kapcsolattarto
    {
        $this->felhasznalo_id = $felhasznalo_id;
        return $this;
    }

    public function getNev(): ?string
    {
        return $this->nev;
    }

    public function setNev(string $nev): static
    {
        $this->nev = $nev;

        return $this;
    }

    public function getJelszo(): ?string
    {
        return $this->jelszo;
    }

    public function setJelszo(string $jelszo): static
    {
        $this->jelszo = $jelszo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getJogosultsag(): array
    {
        return $this->jogosultsag;
    }

    public function setJogosultsag(array $jogosultsag): static
    {
        $this->jogosultsag = $jogosultsag;

        return $this;
    }

    public function getBeosztas(): ?string
    {
        return $this->beosztas;
    }

    public function setBeosztas(string $beosztas): static
    {
        $this->beosztas = $beosztas;

        return $this;
    }
}

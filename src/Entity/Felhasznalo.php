<?php

namespace App\Entity;

use App\Repository\FelhasznaloRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: FelhasznaloRepository::class)]
class Felhasznalo implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $felhasznalo_id = null;

    #[ORM\Column(length: 50)]
    private ?string $nev = null;

    #[ORM\Column(length: 255)]
    private ?string $jelszo = null;

    #[ORM\Column(length: 50, unique: true)]
    private ?string $email = null;

    #[ORM\Column(type: "json", nullable: false)]
    private array $jogosultsag = [];

    public function getFelhasznaloId(): ?int
    {
        return $this->felhasznalo_id;
    }

    public function setFelhasznaloId(int $felhasznalo_id): static
    {
        $this->felhasznalo_id = $felhasznalo_id;

        return $this;
    }

    public function getNev(): ?string
    {
        return $this->nev;
    }

    public function setNev(string $nev): static
    {
        $this->nev = $nev;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->jelszo;
    }

    public function setPassword(string $password): self
    {
        $this->jelszo = $password;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->jogosultsag;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->jogosultsag = $roles;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
    }
}

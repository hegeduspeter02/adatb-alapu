<?php

namespace App\Entity;

use App\Repository\JelentkezesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: JelentkezesRepository::class)]
class Jelentkezes
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $jelentkezes_id = null;

    #[ManyToOne(targetEntity: Felhasznalo::class)]
    #[JoinColumn(name: 'felhasznalo_id', referencedColumnName: 'felhasznalo_id', nullable: false)]
    private ?Felhasznalo $felhasznalo = null;

    #[ManyToOne(targetEntity: Pozicio::class)]
    #[JoinColumn(name: 'pozicio_id', referencedColumnName: 'pozicio_id', nullable: false)]
    private ?Pozicio $pozicio = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $uzenet = null;

    #[ORM\Column(nullable: true)]
    private ?int $elutasitva = null;

    public function getJelentkezesId(): ?int
    {
        return $this->jelentkezes_id;
    }

    public function setJelentkezesId(?int $jelentkezes_id): void
    {
        $this->jelentkezes_id = $jelentkezes_id;
    }

    public function getFelhasznalo(): ?Felhasznalo
    {
        return $this->felhasznalo;
    }

    public function setFelhasznalo(?Felhasznalo $felhasznalo): Jelentkezes
    {
        $this->felhasznalo = $felhasznalo;
        return $this;
    }

    public function getPozicio(): ?Pozicio
    {
        return $this->pozicio;
    }

    public function setPozicio(?Pozicio $pozicio): Jelentkezes
    {
        $this->pozicio = $pozicio;
        return $this;
    }

    public function getUzenet(): ?string
    {
        return $this->uzenet;
    }

    public function setUzenet(?string $uzenet): Jelentkezes
    {
        $this->uzenet = $uzenet;
        return $this;
    }

    public function getElutasitva(): ?int
    {
        return $this->elutasitva;
    }

    public function setElutasitva(?int $elutasitva): Jelentkezes
    {
        $this->elutasitva = $elutasitva;
        return $this;
    }

}

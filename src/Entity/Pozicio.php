<?php

namespace App\Entity;

use App\Repository\PozicioRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: PozicioRepository::class)]
class Pozicio
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $pozicio_id = null;

    #[ManyToOne(targetEntity: MunkaTipusa::class)]
    #[JoinColumn(name: 'munka_tipus_id', referencedColumnName: 'munka_tipus_id', nullable: false)]
    private ?MunkaTipusa $munkaTipusa = null;

    #[ManyToOne(targetEntity: Ceg::class)]
    #[JoinColumn(name: 'ceg_id', referencedColumnName: 'ceg_id', nullable: false)]
    private ?Ceg $ceg = null;

    #[ORM\Column(length: 30)]
    private ?string $feladatkor = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $leiras = null;

    #[ORM\Column(length: 255)]
    private ?string $elvaras = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $kinalat = null;

    public function getPozicioId(): ?int
    {
        return $this->pozicio_id;
    }
    public function setPozicioID(?int $id): ?int
    {
        $this->pozicio_id=$id;
        return $id;
    }
    public function getMunkaTipusa(): ?MunkaTipusa
    {
        return $this->munkaTipusa;
    }

    public function setMunkaTipusa(?MunkaTipusa $munkaTipusa): Pozicio
    {
        $this->munkaTipusa = $munkaTipusa;
        return $this;
    }

    public function getCeg(): ?Ceg
    {
        return $this->ceg;
    }

    public function setCeg(?Ceg $ceg): Pozicio
    {
        $this->ceg = $ceg;
        return $this;
    }

    public function getFeladatkor(): ?string
    {
        return $this->feladatkor;
    }

    public function setFeladatkor(string $feladatkor): static
    {
        $this->feladatkor = $feladatkor;

        return $this;
    }

    public function getLeiras(): ?string
    {
        return $this->leiras;
    }

    public function setLeiras(?string $leiras): static
    {
        $this->leiras = $leiras;

        return $this;
    }

    public function getElvaras(): ?string
    {
        return $this->elvaras;
    }

    public function setElvaras(string $elvaras): static
    {
        $this->elvaras = $elvaras;

        return $this;
    }

    public function getKinalat(): ?string
    {
        return $this->kinalat;
    }

    public function setKinalat(string $kinalat): static
    {
        $this->kinalat = $kinalat;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\TanulmanyiStatuszRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TanulmanyiStatuszRepository::class)]
class TanulmanyiStatusz
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(nullable: false)]
    private ?int $tan_statusz_id = null;

    #[ORM\Column(length: 30)]
    private ?string $megnevezes = null;

    public function getTanStatuszId(): ?int
    {
        return $this->tan_statusz_id;
    }

    public function getMegnevezes(): ?string
    {
        return $this->megnevezes;
    }

    public function setMegnevezes(string $megnevezes): static
    {
        $this->megnevezes = $megnevezes;

        return $this;
    }

    public function __toString(): string
    {
        return $this->megnevezes;
    }
}

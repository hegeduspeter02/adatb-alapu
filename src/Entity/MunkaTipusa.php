<?php

namespace App\Entity;

use App\Repository\MunkaTipusaRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MunkaTipusaRepository::class)]
class MunkaTipusa
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(nullable: false)]
    private ?int $munka_tipus_id = null;

    #[ORM\Column(length: 30)]
    private ?string $megnevezes = null;

    public function getMunkaTipusId(): ?int
    {
        return $this->munka_tipus_id;
    }

    public function getMegnevezes(): ?string
    {
        return $this->megnevezes;
    }

    public function setMegnevezes(string $megnevezes): static
    {
        $this->megnevezes = $megnevezes;

        return $this;
    }
}

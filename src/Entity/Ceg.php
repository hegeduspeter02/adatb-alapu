<?php

namespace App\Entity;

use App\Repository\CegRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: CegRepository::class)]
class Ceg
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $ceg_id = null;

    #[ManyToOne(targetEntity: Felhasznalo::class)]
    #[JoinColumn(name: 'felhasznalo_id', referencedColumnName: 'felhasznalo_id', nullable: false)]
    private ?Felhasznalo $kapcsolattarto = null;

    #[ORM\Column(length: 50)]
    private ?string $nev = null;

    #[ORM\Column(length: 20)]
    private ?string $orszag = null;

    #[ORM\Column(length: 20)]
    private ?string $varos = null;

    #[ORM\Column(length: 10)]
    private ?string $iranyitoszam = null;

    #[ORM\Column(length: 100)]
    private ?string $cim = null;

    #[ORM\Column]
    private ?int $adoszam = null;

    public function getCegId(): ?int
    {
        return $this->ceg_id;
    }

    public function setCegId(?int $ceg_id): Ceg
    {
        $this->ceg_id = $ceg_id;
        return $this;
    }

    public function getKapcsolattarto(): ?Felhasznalo
    {
        return $this->kapcsolattarto;
    }

    public function setKapcsolattarto(?Felhasznalo $kapcsolattarto): Ceg
    {
        $this->kapcsolattarto = $kapcsolattarto;
        return $this;
    }

    public function getNev(): ?string
    {
        return $this->nev;
    }

    public function setNev(string $nev): static
    {
        $this->nev = $nev;

        return $this;
    }

    public function getOrszag(): ?string
    {
        return $this->orszag;
    }

    public function setOrszag(string $orszag): static
    {
        $this->orszag = $orszag;

        return $this;
    }

    public function getVaros(): ?string
    {
        return $this->varos;
    }

    public function setVaros(?string $varos): Ceg
    {
        $this->varos = $varos;
        return $this;
    }

    public function getIranyitoszam(): ?string
    {
        return $this->iranyitoszam;
    }

    public function setIranyitoszam(string $iranyitoszam): static
    {
        $this->iranyitoszam = $iranyitoszam;

        return $this;
    }

    public function getCim(): ?string
    {
        return $this->cim;
    }

    public function setCim(string $cim): static
    {
        $this->cim = $cim;

        return $this;
    }

    public function getAdoszam(): ?int
    {
        return $this->adoszam;
    }

    public function setAdoszam(?int $adoszam): static
    {
        $this->adoszam = $adoszam;
        return $this;
    }

}

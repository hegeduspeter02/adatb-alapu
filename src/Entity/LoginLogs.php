<?php

namespace App\Entity;

use App\Repository\LoginLogsRepository;
use App\Repository\PozicioLogsRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: LoginLogsRepository::class)]
class LoginLogs
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $login_log_id = null;

    #[ManyToOne(targetEntity: Felhasznalo::class)]
    #[JoinColumn(name: 'felhasznalo_id', referencedColumnName: 'felhasznalo_id', nullable: false)]
    private ?Felhasznalo $felhasznalo = null;


    #[ORM\Column(nullable: false)]
    private ?string $loginTime = null;

    public function getLoginLogId(): ?int
    {
        return $this->login_log_id;
    }

    public function getLoginTime(): ?string
    {
        return $this->loginTime;
    }

    public function getFelhasznalo(): ?Felhasznalo
    {
        return $this->felhasznalo;
    }

}

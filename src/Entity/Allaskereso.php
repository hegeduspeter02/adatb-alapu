<?php

namespace App\Entity;

use App\Repository\AllaskeresoRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: AllaskeresoRepository::class)]
class Allaskereso
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $felhasznalo_id = null;

    #[ManyToOne(targetEntity: TanulmanyiStatusz::class)]
    #[JoinColumn(name: 'tan_statusz_id', referencedColumnName: 'tan_statusz_id', nullable: false)]
    private ?TanulmanyiStatusz $tan_statusz = null;

    #[ORM\Column(length: 50)]
    private ?string $nev = null;

    #[ORM\Column(length: 255)]
    private ?string $jelszo = null;

    #[ORM\Column(length: 50)]
    private ?string $email = null;

    #[ORM\Column(type: "json", nullable: false)]
    private array $jogosultsag = [];

    #[ORM\Column(length: 100)]
    private ?string $varos = null;

    #[ORM\Column(length: 15, nullable: true)]
    private ?string $telefonszam = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $oneletrajz = null;

    public function getFelhasznaloId(): ?int
    {
        return $this->felhasznalo_id;
    }

    public function setFelhasznaloId(?int $felhasznalo_id): Allaskereso
    {
        $this->felhasznalo_id = $felhasznalo_id;
        return $this;
    }

    public function getTanStatusz(): ?TanulmanyiStatusz
    {
        return $this->tan_statusz;
    }

    public function setTanStatusz(?TanulmanyiStatusz $tan_statusz): Allaskereso
    {
        $this->tan_statusz = $tan_statusz;
        return $this;
    }

    public function getNev(): ?string
    {
        return $this->nev;
    }

    public function setNev(?string $nev): Allaskereso
    {
        $this->nev = $nev;
        return $this;
    }

    public function getJelszo(): ?string
    {
        return $this->jelszo;
    }

    public function setJelszo(string $jelszo): static
    {
        $this->jelszo = $jelszo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getJogosultsag(): array
    {
        return $this->jogosultsag;
    }

    public function setJogosultsag(array $jogosultsag): static
    {
        $this->jogosultsag = $jogosultsag;

        return $this;
    }

    public function getVaros(): ?string
    {
        return $this->varos;
    }

    public function setVaros(?string $varos): Allaskereso
    {
        $this->varos = $varos;
        return $this;
    }

    public function getTelefonszam(): ?string
    {
        return $this->telefonszam;
    }

    public function setTelefonszam(?string $telefonszam): static
    {
        $this->telefonszam = $telefonszam;

        return $this;
    }

    public function getOneletrajz(): ?string
    {
        return $this->oneletrajz;
    }

    public function setOneletrajz(?string $oneletrajz): static
    {
        $this->oneletrajz = $oneletrajz;

        return $this;
    }
}

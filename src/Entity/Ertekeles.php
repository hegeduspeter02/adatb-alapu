<?php

namespace App\Entity;

use App\Repository\ErtekelesRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: ErtekelesRepository::class)]
class Ertekeles
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $ertekeles_id = null;

    #[ManyToOne(targetEntity: Felhasznalo::class)]
    #[JoinColumn(name: 'felhasznalo_id', referencedColumnName: 'felhasznalo_id', nullable: false)]
    private ?Felhasznalo $felhasznalo = null;

    #[ManyToOne(targetEntity: Ceg::class)]
    #[JoinColumn(name: 'CEG_ID', referencedColumnName: 'ceg_id', nullable: false)]
    private ?Ceg $ceg = null;

    #[ORM\Column]
    private ?int $pontszam = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $szoveg = null;

    public function getId(): ?int
    {
        return $this->ertekeles_id;
    }

    public function getErtekelesId(): ?int
    {
        return $this->ertekeles_id;
    }

    public function setErtekelesId(int $ertekeles_id): static
    {
        $this->ertekeles_id = $ertekeles_id;

        return $this;
    }

    public function getFelhasznalo(): ?Felhasznalo
    {
        return $this->felhasznalo;
    }

    public function setFelhasznalo(?Felhasznalo $felhasznalo): Ertekeles
    {
        $this->felhasznalo = $felhasznalo;
        return $this;
    }

    public function getCeg(): ?Ceg
    {
        return $this->ceg;
    }

    public function setCeg(?Ceg $ceg): Ertekeles
    {
        $this->ceg = $ceg;
        return $this;
    }

    public function getPontszam(): ?int
    {
        return $this->pontszam;
    }

    public function setPontszam(int $pontszam): static
    {
        $this->pontszam = $pontszam;

        return $this;
    }

    public function getSzoveg(): ?string
    {
        return $this->szoveg;
    }

    public function setSzoveg(?string $szoveg): static
    {
        $this->szoveg = $szoveg;

        return $this;
    }
}

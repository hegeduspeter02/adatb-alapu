<?php

namespace App\Entity;

use App\Repository\PozicioLogsRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

#[ORM\Entity(repositoryClass: PozicioLogsRepository::class)]
class PozicioLogs
{
    #[ORM\Id]
    #[ORM\Column(nullable: false)]
    private ?int $log_id = null;

    #[ManyToOne(targetEntity: Ceg::class)]
    #[JoinColumn(name: 'ceg_id', referencedColumnName: 'ceg_id', nullable: false)]
    private ?Ceg $ceg = null;

    #[ManyToOne(targetEntity: MunkaTipusa::class)]
    #[JoinColumn(name: 'munka_tipusa_id_old', referencedColumnName: 'munka_tipus_id', nullable: false)]
    private ?MunkaTipusa $munkaTipusaOld = null;

    #[ManyToOne(targetEntity: MunkaTipusa::class)]
    #[JoinColumn(name: 'munka_tipusa_id_new', referencedColumnName: 'munka_tipus_id', nullable: false)]
    private ?MunkaTipusa $munkaTipusaNew = null;

    #[ORM\Column(length: 30)]
    private ?string $feladatkorOld = null;

    #[ORM\Column(length: 30)]
    private ?string $feladatkorNew = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $leirasOld = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $leirasNew = null;

    #[ORM\Column(length: 255)]
    private ?string $elvarasOld = null;

    #[ORM\Column(length: 255)]
    private ?string $elvarasNew = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $kinalatOld = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $kinalatNew = null;

    #[ORM\Column(nullable: false)]
    private ?string $editTime = null;

    public function getLogId(): ?int
    {
        return $this->log_id;
    }

    public function getEditTime(): ?string
    {
        return $this->editTime;
    }

    public function setEditTime(?string $editTime): PozicioLogs
    {
        $this->editTime = $editTime;
        return $this;
    }

    public function getCeg(): ?Ceg
    {
        return $this->ceg;
    }

    public function getMunkaTipusaOld(): ?MunkaTipusa
    {
        return $this->munkaTipusaOld;
    }

    public function getMunkaTipusaNew(): ?MunkaTipusa
    {
        return $this->munkaTipusaNew;
    }

    public function getFeladatkorOld(): ?string
    {
        return $this->feladatkorOld;
    }

    public function getFeladatkorNew(): ?string
    {
        return $this->feladatkorNew;
    }

    public function getLeirasOld(): ?string
    {
        return $this->leirasOld;
    }

    public function getLeirasNew(): ?string
    {
        return $this->leirasNew;
    }

    public function getElvarasOld(): ?string
    {
        return $this->elvarasOld;
    }

    public function getElvarasNew(): ?string
    {
        return $this->elvarasNew;
    }

    public function getKinalatOld(): ?string
    {
        return $this->kinalatOld;
    }

    public function getKinalatNew(): ?string
    {
        return $this->kinalatNew;
    }

}

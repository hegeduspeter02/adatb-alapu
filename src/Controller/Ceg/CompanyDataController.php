<?php

namespace App\Controller\Ceg;

use App\Entity\Ceg;
use App\Entity\Felhasznalo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CompanyDataController extends AbstractController
{
    #[Route('/company/data', name: 'app_company_data')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $kapcsolattarto = $em->getRepository(Felhasznalo::class)->findOneBy(['felhasznalo_id' => $this->getUser()->getFelhasznaloId()]);

        $ceg = $em->getRepository(Ceg::class)->findOneBy(['kapcsolattarto' => $kapcsolattarto]);
        if($ceg == null){
            $ceg = new Ceg();
            $ceg->setKapcsolattarto($kapcsolattarto);
        }

        $form = $this->createFormBuilder($ceg)
            ->add('nev', TextType::class,[
                'required' => true,
            ])
            ->add('orszag', TextType::class,[
                'required' => true,
            ])
            ->add('varos', TextType::class,[
                'required' => true,
            ])
            ->add('iranyitoszam', TextType::class,[
                'required' => true,
            ])
            ->add('cim', TextType::class,[
                'required' => true,
            ])
            ->add('adoszam', IntegerType::class,[
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Mentés'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ceg = $form->getData();

            $previousCeg = $em->getRepository(Ceg::class)->findOneBy(['kapcsolattarto' => $kapcsolattarto]);
            $latestCeg = $em->getRepository(Ceg::class)->findOneBy([], ['ceg_id' => 'desc']);

            if(!$previousCeg){
                $ceg->setCegId($latestCeg->getCegId() + 1);

                $em->persist($ceg);
                $em->flush();
            }
            else{
                $cegRepository = $em->getRepository(Ceg::class);
                $cegRepository->updateCeg($ceg, $ceg->getCegId());
            }

            $this->addFlash('adatmodositas-success', 'Sikeres adatmódosítás!');

            return $this->redirectToRoute('app_company_data');
        }

        return $this->render('company_data/company.data.html.twig', [
            'form' => $form,
        ]);
    }
}

<?php

namespace App\Controller\Jelentkezesek;

use App\Entity\Ceg;
use App\Entity\Jelentkezes;
use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ApplicationController extends AbstractController
{
    #[Route('/application', name: 'app_application')]
    public function index(EntityManagerInterface $entityManager,): Response
    {

        $role = $this->getUser()->getRoles();

        if(in_array("ROLE_ALLASKERESO",$role))
            $jelentkezesek = $entityManager ->getRepository(Jelentkezes::class)->findBy(['felhasznalo' => $this->getUser()->getFelhasznaloId()]);
        else if(in_array("ROLE_KAPCSOLATTARTO",$role)) {
            $ceg = $entityManager->getRepository(Ceg::class)->findOneBy(['kapcsolattarto' => $this->getUser()]);
            $cegpoziciok=$entityManager->getRepository(Pozicio::class)->findBy(['ceg'=>$ceg->getCegId()]);
            $jelentkezesek=$entityManager->getRepository(Jelentkezes::class)->findBy(['pozicio'=>$cegpoziciok]);
        }
        else if(in_array("ROLE_ADMIN",$role)) {
            $jelentkezesek=$entityManager->getRepository(Jelentkezes::class)->findAll();

        }





        return $this->render('application/application.html.twig', [
            'jelentkezesek' => $jelentkezesek,
        ]);
    }
    #[Route('/application/delete/{application_id}', name: 'app_application_delete')]
    public function deleteApplication(int $application_id, EntityManagerInterface $entityManager): Response
    {
        $application = $entityManager->getRepository(Jelentkezes::class)->findOneBy(['jelentkezes_id' => $application_id]);
            $this->addFlash('delete-success', 'Jelentkezés törölve!');
            $entityManager->remove($application);
            $entityManager->flush();
            return $this->redirectToRoute('app_application');
    }
    #[Route('/application/accept/{application_id}', name: 'app_application_accept')]
    public function acceptApplication(int $application_id, EntityManagerInterface $entityManager): Response
    {
        $application = $entityManager->getRepository(Jelentkezes::class)->findOneBy(['jelentkezes_id' => $application_id]);
        if ($application) {
            $application->setElutasitva(false);
            $this->addFlash('accept-success', 'Jelentkezés elfogadva!');
            $entityManager->persist($application);
            $entityManager->flush();
        }
        return $this->redirectToRoute('app_application');
    }
    #[Route('/application/refuse/{application_id}', name: 'app_application_refuse')]
    public function refuseApplication(int $application_id, EntityManagerInterface $entityManager): Response
    {
        $application = $entityManager->getRepository(Jelentkezes::class)->findOneBy(['jelentkezes_id' => $application_id]);
        if ($application) {
            $application->setElutasitva(true);
            $this->addFlash('accept-success', 'Jelentkezés elfogadva!');
            $entityManager->persist($application);
            $entityManager->flush();
        }
        return $this->redirectToRoute('app_application');
    }




}

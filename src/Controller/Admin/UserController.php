<?php

namespace App\Controller\Admin;

use App\Entity\Allaskereso;
use App\Entity\Felhasznalo;
use App\Entity\Kapcsolattarto;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class UserController extends AbstractController
{
    #[Route('/users', name: 'app_users')]
    public function index(EntityManagerInterface $em): Response
    {
        $users = $em->getRepository(Felhasznalo::class)->findAll();

        return $this->render('user/user.html.twig', [
            'users' => $users,
        ]);
    }

    #[Route('/users/delete/{user_id}', name: 'app_user_delete')]
    public function deleteUser(int $user_id, EntityManagerInterface $em): Response
    {
        $user = $em->getRepository(Felhasznalo::class)->findOneBy(['felhasznalo_id' => $user_id]);
        if ($user) {
            if ($user->getRoles() == ['ROLE_ALLASKERESO']) {
                $allaskeresoUser = $em->getRepository(Allaskereso::class)->findOneBy(['felhasznalo_id' => $user_id]);
                $em->remove($allaskeresoUser);
                $em->flush();
            } else if ($user->getRoles() == ['ROLE_KAPCSOLATTARTO']) {
                $kapcsolattartoUser = $em->getRepository(Kapcsolattarto::class)->findOneBy(['felhasznalo_id' => $user_id]);
                $em->remove($kapcsolattartoUser);
                $em->flush();
            }

            $this->addFlash('delete-success', 'Felhasználó sikeresen törölve!');

            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('app_users');
    }
}

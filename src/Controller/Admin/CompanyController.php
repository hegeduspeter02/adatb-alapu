<?php

namespace App\Controller\Admin;

use App\Entity\Ceg;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CompanyController extends AbstractController
{
    #[Route('/companies', name: 'app_companies')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $cegek = $em->getRepository(Ceg::class)->findAll();

        $forms = [];
        foreach ($cegek as $ceg) {
            $form = $this->createFormBuilder($ceg)
                ->add('nev', TextType::class, [
                    'required' => true,
                ])
                ->add('orszag', TextType::class, [
                    'required' => true,
                ])
                ->add('varos', TextType::class, [
                    'required' => true,
                ])
                ->add('iranyitoszam', TextType::class, [
                    'required' => true,
                ])
                ->add('cim', TextType::class, [
                    'required' => true,
                ])
                ->add('adoszam', IntegerType::class, [
                    'required' => true,
                ])
                ->add('cegId', HiddenType::class, [
                    'data' => $ceg->getCegId(),
                ])
                ->add('save', SubmitType::class, ['label' => 'Mentés'])
                ->add('delete', SubmitType::class, ['label' => 'Cég törlése'])
                ->getForm();

            $forms[$ceg->getCegId()] = $form;
        }

        foreach ($forms as $form) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $cegId = $form->getData()->getCegId();
                $ceg = $form->getData();

                $cegRepository = $em->getRepository(Ceg::class);
                if ($form->get('delete')->isClicked()) {
                    $cegRepository->deleteCeg($cegId);

                    $this->addFlash('adatmodositas-success', 'Cég sikeresen törölve!');
                }
                else{
                    $cegRepository->updateCeg($ceg, $cegId);

                    $this->addFlash('adatmodositas-success', 'Sikeres adatmódosítás!');
                }

                return $this->redirectToRoute('app_companies');
            }
        }

        return $this->render('company/company.html.twig', [
            'cegek' => $cegek,
            'forms' => array_map(function($form) {
                return $form->createView();
            }, $forms),
        ]);
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Jelentkezes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdminApplicationController extends AbstractController
{
    #[Route('/admin-applications', name: 'app_applications')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $jelentkezesek = $em->getRepository(Jelentkezes::class)->findAll();

        $forms = [];
        foreach ($jelentkezesek as $jelentkezes) {
            $form = $this->createFormBuilder($jelentkezes)
                ->add('delete', SubmitType::class, ['label' => 'Jelentkezés törlése'])
                ->getForm();

            $forms[$jelentkezes->getJelentkezesId()] = $form;
        }

        foreach ($forms as $form) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $jelentkezesid = $form->getData()->getJelentkezesId();

                $jelentkezesRepository = $em->getRepository(Jelentkezes::class);
                if ($form->get('delete')->isClicked()) {
                    $jelentkezesRepository->deleteJelentkezes($jelentkezesid);

                    $this->addFlash('adatmodositas-success', 'Jelentkezés sikeresen törölve!');
                }
                return $this->redirectToRoute('app_applications');
            }
        }

        return $this->render('admin-application/admin-application.html.twig', [
            'jelentkezesek' => $jelentkezesek,
            'forms' => array_map(function($form) {
                return $form->createView();
            }, $forms),
        ]);
    }
}

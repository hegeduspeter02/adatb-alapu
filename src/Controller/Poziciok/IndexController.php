<?php

namespace App\Controller\Poziciok;

use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function listPositions(EntityManagerInterface $em): Response
    {
        $poziciok = $em->getRepository(Pozicio::class)->findAll();

        $groupedPoziciok = [];
        foreach ($poziciok as $pozicio) {
            $groupedPoziciok[$pozicio->getFeladatkor()][] = $pozicio;
        }

        return $this->render('index/index.html.twig', [
            'poziciok' => $poziciok,
            'groupedPoziciok' => $groupedPoziciok,
        ]);
    }
}

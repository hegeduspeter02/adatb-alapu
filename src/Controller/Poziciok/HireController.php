<?php

namespace App\Controller\Poziciok;

use App\Entity\Ceg;
use App\Entity\MunkaTipusa;
use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HireController extends AbstractController
{
    #[Route('/hire', name: 'app_hire')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {

        $pozicio = new Pozicio();
        $ceg = $em->getRepository(Ceg::class)->findOneBy(['kapcsolattarto' => $this->getUser()]);
        $pozicio->setCeg($ceg);
        $munkatipusok=$em->getRepository(MunkaTipusa::class)->findAll();
        dump($munkatipusok);
        $types=array();
        foreach ($munkatipusok as &$munka){
            $types[] = array($munka->getMegnevezes() => $munka->getMunkaTipusId());
        }

        $jobform = $this->createFormBuilder()
            ->add('feladatkor', TextareaType::class, ['label' =>'Feladatkör'])
            ->add('leiras', TextareaType::class, ['label' =>'Leírás'])
            ->add('elvaras', TextareaType::class, ['label' =>'Elvárás'])
            ->add('kinalat', TextareaType::class, ['label'=>'Kínálat'])
            ->add('munkatipus', ChoiceType::class, ['choices' =>$types])
            ->add('meghirdet', SubmitType::class, ['label' =>'Meghírdet'])
            ->getForm();



        $jobform->handleRequest($request);
        if($jobform->isSubmitted() && $jobform->isValid()){
            $pozicio->setFeladatkor($jobform['feladatkor']->getData());
            $pozicio->setElvaras($jobform['elvaras']->getData());
            $pozicio->setKinalat($jobform['kinalat']->getData());
            $pozicio->setLeiras($jobform['leiras']->getData());

            $munkatipus=$em->getRepository(MunkaTipusa::class)->findOneBy(['munka_tipus_id'=>$jobform['munkatipus']->getData()]);
            $pozicio->setMunkaTipusa($munkatipus);

            $elozopozi = $em->getRepository(Pozicio::class)->findOneBy([], ['pozicio_id' => 'desc']);
            $nextId = $elozopozi ? $elozopozi->getPozicioId() + 1 : 1;
            $pozicio->setPozicioID($nextId);
            $em->persist($pozicio);
            $em->flush();
            return $this->redirectToRoute('app_index');
        }






        return $this->render('hire/hire.html.twig', [
            'hire' => 'HireController',
            'jobform' =>$jobform
        ]);




    }
}

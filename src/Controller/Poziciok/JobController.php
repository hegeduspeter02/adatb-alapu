<?php


namespace App\Controller\Poziciok;

use App\Entity\Ceg;
use App\Entity\Ertekeles;
use App\Entity\Felhasznalo;
use App\Entity\Jelentkezes;
use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class JobController extends AbstractController
{
    #[Route('/job/{id}', name: 'show_pozicio')]
    public function showPositionData($id, Request $request, EntityManagerInterface $em){
        $pozicio = $em->getRepository(Pozicio::class)->find($id);
        $user = $this->getUser();
        $ertekelesek = $em->getRepository(Ertekeles::class)->findBy(["ceg" => $pozicio->getCeg()]);

        if ($user) {
            $roles = $user->getRoles();
            if (in_array('ROLE_ALLASKERESO', $roles)) {
                return $this->allaskereso($pozicio, $ertekelesek, $request, $em);
            }
        }

        return $this->render('job/job.html.twig', [
            'pozicio' => $pozicio,
            'ertekelesek' => $ertekelesek
        ]);
    }

    public function allaskereso(Pozicio $pozicio, $ertekelesek, Request $request, EntityManagerInterface $em){
        $allaskereso = $em->getRepository(Felhasznalo::class)->findOneBy(['felhasznalo_id' => $this->getUser()->getFelhasznaloId()]);
        $existingJelentkezes = $em->getRepository(Jelentkezes::class)->findOneBy(['felhasznalo' => $allaskereso, 'pozicio' => $pozicio]);
        $jelentkezes = new Jelentkezes();
        $jelentkezes->setFelhasznalo($allaskereso);
        $jelentkezes->setPozicio($pozicio);

        $jelform = $this->createFormBuilder($jelentkezes)
            ->add('uzenet', TextareaType::class, [
                'required' => false,
            ])
            ->add('apply', SubmitType::class, ['label' => 'Jelentkezés'])
            ->getForm();

        $jelform->handleRequest($request);
        if ($jelform->isSubmitted() && $jelform->isValid()) {
            if ($existingJelentkezes) {
                $existingJelentkezes->setUzenet($jelform['uzenet']->getData());
                $em->flush();
            }else {
                $jelentkezes->setUzenet($jelform['uzenet']->getData());

                $prevJelentkezes = $em->getRepository(Jelentkezes::class)->findOneBy([], ['jelentkezes_id' => 'desc']);
                $nextId = $prevJelentkezes ? $prevJelentkezes->getJelentkezesId() + 1 : 1;

                //$userId = $allaskereso->getFelhasznaloId();
                //$pozicioId = $pozicio->getPozicioId();
                //$jelentkezesId = intval($userId . $pozicioId);

                $jelentkezes->setJelentkezesId($nextId);

                $em->persist($jelentkezes);
                $em->flush();
            }
        }

        $ceg = $em->getRepository(Ceg::class)->find($pozicio->getCeg()->getCegId());
        $existingErtekeles = $em->getRepository(Ertekeles::class)->findOneBy(['felhasznalo' => $allaskereso, 'ceg' => $ceg]);
        $ertekeles = new Ertekeles();
        $ertekeles->setFelhasznalo($allaskereso);
        $ertekeles->setCeg($ceg);


        $ertform = $this->createFormBuilder($ertekeles)
            ->add('szoveg', TextareaType::class, [
                'required' => false,
            ])
            ->add('pontszam', IntegerType::class, [
                'required' => true,
            ])
            ->add('send', SubmitType::class, ['label' => 'Értékelés'])
            ->getForm();

        $ertform->handleRequest($request);
        if ($ertform->isSubmitted() && $ertform->isValid()) {
            $prevErtekeles = $em->getRepository(Ertekeles::class)->findOneBy([], ['ertekeles_id' => 'desc']);
            $nextId = $prevErtekeles ? $prevErtekeles->getErtekelesId() + 1 : 1;

            $ertekeles->setErtekelesId($nextId);
            $ertekeles->setPontszam($ertform['pontszam']->getData());
            $ertekeles->setSzoveg($ertform['szoveg']->getData());

            $em->getRepository(Ertekeles::class)->addErtekelesWithDBProcedure($ertekeles);

            $ertekelesek = $em->getRepository(Ertekeles::class)->findBy(["ceg" => $pozicio->getCeg()]);
        }

        return $this->render('job/job.html.twig', [
            'pozicio' => $pozicio,
            'ertekelesek' => $ertekelesek,
            'jelform' => $jelform,
            'ertform' => $ertform,
            'ceg' => $ceg,
        ]);
    }
}


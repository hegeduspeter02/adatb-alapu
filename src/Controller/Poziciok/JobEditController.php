<?php

namespace App\Controller\Poziciok;

use App\Entity\Ceg;
use App\Entity\Kapcsolattarto;
use App\Entity\MunkaTipusa;
use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class JobEditController extends AbstractController
{
    #[Route('/job-edit', name: 'app_job_edit')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {
        $role = $this->getUser()->getRoles();

        if(in_array("ROLE_KAPCSOLATTARTO",$role)){
            $kapcsolattarto = $em->getRepository(Kapcsolattarto::class)->findOneBy(['felhasznalo_id' => $this->getUser()->getFelhasznaloId()]);
            $ceg = $em->getRepository(Ceg::class)->findOneBy(['kapcsolattarto' => $kapcsolattarto]);
            $poziciok = $em->getRepository(Pozicio::class)->findBy(['ceg' => $ceg]);
        }
        else if(in_array("ROLE_ADMIN",$role)){
            $poziciok = $em->getRepository(Pozicio::class)->findAll();
        }

        $munkaTipusaRepository = $em->getRepository(MunkaTipusa::class);
        $munkaTipusaChoices = $munkaTipusaRepository->findAll();

        $munkaTipusaChoicesArray = [];
        foreach ($munkaTipusaChoices as $munkaTipusa) {
            $munkaTipusaChoicesArray[$munkaTipusa->getMegnevezes()] = $munkaTipusa;
        }

        $forms = [];
        foreach ($poziciok as $pozicio) {
            $form = $this->createFormBuilder($pozicio)
                ->add('munka_tipusa', ChoiceType::class, [
                    'choices' => $munkaTipusaChoicesArray,
                ])
                ->add('feladatkor', TextType::class, [
                    'required' => true,
                ])
                ->add('leiras', TextType::class, [
                    'required' => true,
                ])
                ->add('elvaras', TextType::class, [
                    'required' => true,
                ])
                ->add('kinalat', TextType::class, [
                    'required' => true,
                ])
                ->add('pozicioId', HiddenType::class, [
                    'data' => $pozicio->getPozicioId(),
                ])
                ->add('save', SubmitType::class, ['label' => 'Mentés'])
                ->add('delete', SubmitType::class, ['label' => 'Cég törlése'])
                ->getForm();

            $forms[$pozicio->getPozicioId()] = $form;
        }

        foreach ($forms as $form) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $pozicioId = $form->getData()->getPozicioId();
                $pozicio = $form->getData();

                $pozicioRepository = $em->getRepository(Pozicio::class);
                if ($form->get('delete')->isClicked()) {
                    $pozicioRepository->deletePozicio($pozicioId);

                    $this->addFlash('adatmodositas-success', 'Cég sikeresen törölve!');
                } else {
                    $pozicioRepository->updatePozicio($pozicio, $pozicioId);

                    $this->addFlash('adatmodositas-success', 'Sikeres adatmódosítás!');
                }

                return $this->redirectToRoute('app_job_edit');
            }
        }

        return $this->render('job_edit/job.edit.html.twig', [
            'poziciok' => $poziciok,
            'forms' => array_map(function ($form) {
                return $form->createView();
            }, $forms),
        ]);
    }
}

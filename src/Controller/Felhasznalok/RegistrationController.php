<?php

namespace App\Controller\Felhasznalok;

use App\Entity\Allaskereso;
use App\Entity\Felhasznalo;
use App\Entity\Kapcsolattarto;
use App\Entity\TanulmanyiStatusz;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;

class RegistrationController extends AbstractController
{
    #[Route('/registration', name: 'app_registration')]
    public function index(Request $request, UserPasswordHasherInterface $passwordHasher, EntityManagerInterface $em): Response
    {
        $felhasznalo = new Felhasznalo();

        $form = $this->createFormBuilder($felhasznalo)
            ->add('nev', TextType::class)
            ->add('email', TextType::class)
            ->add('password', PasswordType::class)
            ->add('register', SubmitType::class, [
                'label' => 'Fiók regisztrálása'
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $felhasznalo = $form->getData();
            $isAllaskereso = $request->request->get('isAllaskereso');

            // hash the password (based on the security.yaml config for the $felhasznalo class)
            $hashedPassword = $passwordHasher->hashPassword(
                $felhasznalo,
                $felhasznalo->getPassword()
            );
            $felhasznalo->setPassword($hashedPassword);

            if ($isAllaskereso === "allas_kereso") {
                $felhasznalo->setRoles(['ROLE_ALLASKERESO']);
                $allaskereso = $em->getRepository(Allaskereso::class)->findOneBy([], ['felhasznalo_id' => 'desc']);
                $nextId = $allaskereso ? $allaskereso->getFelhasznaloId() + 1 : 1;

                $tan_statusz = $em->getRepository(TanulmanyiStatusz::class)->find(1);

                $allaskereso = new Allaskereso();
                $allaskereso->setFelhasznaloId($nextId);
                $allaskereso->setTanStatusz($tan_statusz);
                $allaskereso->setNev($felhasznalo->getNev());
                $allaskereso->setEmail($felhasznalo->getEmail());
                $allaskereso->setJelszo($felhasznalo->getPassword());
                $allaskereso->setJogosultsag($felhasznalo->getRoles());
                $allaskereso->setVaros("N/A");
                $allaskereso->setOneletrajz("N/A");

                $em->persist($allaskereso);

            } else {
                $felhasznalo->setRoles(['ROLE_KAPCSOLATTARTO']);
                $kapcsolattarto = $em->getRepository(Kapcsolattarto::class)->findOneBy([], ['felhasznalo_id' => 'desc']);
                $nextId = $kapcsolattarto ? $kapcsolattarto->getFelhasznaloId() + 1 : 10001;

                $kapcsolattarto = new Kapcsolattarto();
                $kapcsolattarto->setFelhasznaloId($nextId);
                $kapcsolattarto->setJogosultsag($felhasznalo->getRoles());
                $kapcsolattarto->setNev($felhasznalo->getNev());
                $kapcsolattarto->setEmail($felhasznalo->getEmail());
                $kapcsolattarto->setJelszo($felhasznalo->getPassword());
                $kapcsolattarto->setBeosztas("N/A");

                $em->persist($kapcsolattarto);
            }
            $felhasznalo->setFelhasznaloId($nextId);

            $em->persist($felhasznalo);

            $em->flush();

            $this->addFlash('success', 'Sikeres regisztráció!');
            return $this->redirectToRoute('app_index');
        }

        return $this->render('registration/register.html.twig', [
            'form' => $form,
        ]);
    }
}

<?php

namespace App\Controller\Felhasznalok;

use App\Entity\Allaskereso;
use App\Entity\Kapcsolattarto;
use App\Entity\TanulmanyiStatusz;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'app_profile')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        if ($user) {
            $roles = $user->getRoles();
            if (in_array('ROLE_ALLASKERESO', $roles)) {
                return $this->allaskeresoAdatmodositas($request, $em);
            } elseif (in_array('ROLE_KAPCSOLATTARTO', $roles)) {
                return $this->cegAdatmodositas($request, $em);
            }
        }

        return $this->redirectToRoute('app_login');
    }

    public function allaskeresoAdatmodositas(Request $request, EntityManagerInterface $em):Response
    {
        $allaskereso = $em->getRepository(Allaskereso::class)->findOneBy(['felhasznalo_id' => $this->getUser()->getFelhasznaloId()]);

        $tanulmanyiStatuszRepository = $em->getRepository(TanulmanyiStatusz::class);
        $tanulmanyiStatuszChoices = $tanulmanyiStatuszRepository->findAll();

        $tanulmanyiStatuszChoicesArray = [];
        foreach ($tanulmanyiStatuszChoices as $tanulmanyiStatusz) {
            $tanulmanyiStatuszChoicesArray[$tanulmanyiStatusz->getMegnevezes()] = $tanulmanyiStatusz;
        }

        $form = $this->createFormBuilder($allaskereso)
            ->add('tan_statusz', ChoiceType::class, [
                'choices'  => $tanulmanyiStatuszChoicesArray,
            ])
            ->add('varos', TextType::class)
            ->add('telefonszam', TextType::class,[
                'required' => false,
            ])
            ->add('oneletrajz', FileType::class,[
                'data_class' => null,
                'required' => false
            ])
            ->add('save', SubmitType::class, ['label' => 'Mentés'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $allaskereso = $form->getData();

            if($form['oneletrajz']->getData()){
                $file = $form['oneletrajz']->getData();
                $extension = $file->guessExtension();
                if (!$extension) {
                    $extension = 'bin';
                }
                $file->move('oneletrajz', 'oneletrajz_'.$allaskereso->getFelhasznaloId().'.'.$extension);
                $allaskereso->setOneletrajz('oneletrajz_'.$allaskereso->getFelhasznaloId().'.'.$extension);
            }

            $em->persist($allaskereso);
            $em->flush();

            $this->addFlash('adatmodositas-success', 'Sikeres adatmódosítás!');

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('profile/profile.html.twig', [
            'form' => $form,
        ]);
    }

    public function cegAdatmodositas(Request $request, EntityManagerInterface $em):Response
    {
        $kapcsolattarto = $em->getRepository(Kapcsolattarto::class)->findOneBy(['felhasznalo_id' => $this->getUser()->getFelhasznaloId()]);

        $kapcsolattartoBeosztas = $this->createFormBuilder($kapcsolattarto)
            ->add('beosztas', TextType::class,[
                'required' => false,
            ])
            ->add('save', SubmitType::class, ['label' => 'Mentés'])
            ->getForm();

        $kapcsolattartoBeosztas->handleRequest($request);
        if ($kapcsolattartoBeosztas->isSubmitted() && $kapcsolattartoBeosztas->isValid()) {
            $kapcsolattarto = $kapcsolattartoBeosztas->getData();

            $em->persist($kapcsolattarto);
            $em->flush();

            $this->addFlash('adatmodositas-success', 'Sikeres adatmódosítás!');

            return $this->redirectToRoute('app_profile');
        }

        return $this->render('profile/profile.html.twig', [
            'form' => $kapcsolattartoBeosztas,
        ]);
    }
}

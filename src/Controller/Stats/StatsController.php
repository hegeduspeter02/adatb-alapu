<?php

namespace App\Controller\Stats;

use App\Entity\Allaskereso;
use App\Entity\Ertekeles;
use App\Entity\Jelentkezes;
use App\Entity\Pozicio;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class StatsController extends AbstractController
{
    #[Route('/stats', name: 'app_stats')]
    public function index(EntityManagerInterface $em): Response
    {
        $applicationsPerJobTypeAndResult = $this->applicationsPerJobTypeAndResult($em);
        $applicationsPerJobType = $this->applicationsPerJobType($em);
        $reviewAvgs = $this->reviewAvgs($em);
        $numberofjobs = $this->cegmelok($em);
        $jobseekers=$this->jobSeekersStats($em);
        $applicationsPerUser = $this->applicationsPerUser($em);
        $jobseekers = $this->jobSeekersStats($em);
        $countJobTypePerJob = $this->countJobTypePerJob($em);
        $noApplications = $this->noApplications($em);

        return $this->render('stats/stats.html.twig', [
            'jobStatsAndResult' => $applicationsPerJobTypeAndResult,
            'jobStats' => $applicationsPerJobType,
            'countJobTypePerJob' => $countJobTypePerJob,
            'reviewAvgs' => $reviewAvgs,
            'numberofjobs' => $numberofjobs,
            'jobseekers' =>  $jobseekers,
            'applicationsPerUser' => $applicationsPerUser,
            'noApplications' => $noApplications,
        ]);
    }

    public function applicationsPerJobTypeAndResult(EntityManagerInterface $em): array
    {
        $pozicioRepository = $em->getRepository(Pozicio::class);

        return $pozicioRepository->getApplicationsPerJobTypeAndResult();
    }

    public function applicationsPerJobType(EntityManagerInterface $em): array
    {
        $pozicioRepository = $em->getRepository(Pozicio::class);

        return $pozicioRepository->getApplicationsPerJobType();
    }

    public function countJobTypePerJob(EntityManagerInterface $em): array
    {
        $pozicioRepository = $em->getRepository(Pozicio::class);

        return $pozicioRepository->countJobTypePerJob();
    }

    public function reviewAvgs(EntityManagerInterface $em): array{
        $ertekelesRepository = $em->getRepository(Ertekeles::class);

        return $ertekelesRepository->getReviewAveragesPerCompany();
    }
    public function cegmelok(EntityManagerInterface $em): array
    {
        $PozicioRepository = $em->getRepository(Pozicio::class);

        return $PozicioRepository->getPositionsByCompany();
    }
    public function jobSeekersStats(EntityManagerInterface $em):array{
        $allaskeresoRepository = $em ->getRepository(Allaskereso::class);
        return $allaskeresoRepository->getNumberOfPeopleByStudentStatus();
    }
    public function noApplications(EntityManagerInterface $em){
        $allaskeresoRepository = $em->getRepository(Allaskereso::class);
        return $allaskeresoRepository->getNumberOfPeopleLookingForJobs();
    }

    public function applicationsPerUser(EntityManagerInterface $em): array{
        $jelentkezesRepository = $em->getRepository(Jelentkezes::class);
        return $jelentkezesRepository->getApplicationsPerUser();
    }

}

<?php

namespace App\Controller\Logger;

use App\Entity\LoginLogs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LoginLoggerController extends AbstractController
{
    #[Route('/bejelentkezes-logs', name: 'app_bejelentkezes_logger')]
    public function index(EntityManagerInterface $em): Response
    {
        $loginLogs = $em->getRepository(LoginLogs::class)->findAll();

        return $this->render('bejelentkezes_logger/bejelentkezes.logger.html.twig', [
            'loginLogs' => $loginLogs,
        ]);
    }
}

<?php

namespace App\Controller\Logger;

use App\Entity\PozicioLogs;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PozicioLoggerController extends AbstractController
{
    #[Route('/pozicio-logs', name: 'app_pozicio_logger')]
    public function index(EntityManagerInterface $em): Response
    {
        $pozicioLogs = $em->getRepository(PozicioLogs::class)->findAll();

        foreach ($pozicioLogs as $pozicioLog) {
            $editTime = DateTime::createFromFormat('d-M-y h.i.s.u A', $pozicioLog->getEditTime());
            $pozicioLog->setEditTime($editTime->format('Y-m-d H:i:s'));
        }

        return $this->render('pozicio_logger/pozicio.modositasok.hmtl.twig', [
            'pozicioLogs' => $pozicioLogs,
        ]);
    }
}

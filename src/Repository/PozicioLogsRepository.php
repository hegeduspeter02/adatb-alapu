<?php

namespace App\Repository;

use App\Entity\PozicioLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PozicioLogs>
 *
 * @method PozicioLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method PozicioLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method PozicioLogs[]    findAll()
 * @method PozicioLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PozicioLogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PozicioLogs::class);
    }

//    /**
//     * @return PozicioLogs[] Returns an array of PozicioLogs objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PozicioLogs
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\Kapcsolattarto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Kapcsolattarto>
 *
 * @method Kapcsolattarto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kapcsolattarto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kapcsolattarto[]    findAll()
 * @method Kapcsolattarto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KapcsolattartoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Kapcsolattarto::class);
    }

//    /**
//     * @return Kapcsolattarto[] Returns an array of Kapcsolattarto objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('k')
//            ->andWhere('k.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('k.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Kapcsolattarto
//    {
//        return $this->createQueryBuilder('k')
//            ->andWhere('k.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

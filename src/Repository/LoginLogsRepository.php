<?php

namespace App\Repository;

use App\Entity\Felhasznalo;
use App\Entity\LoginLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LoginLogs>
 *
 * @method LoginLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoginLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoginLogs[]    findAll()
 * @method LoginLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoginLogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LoginLogs::class);
    }

    public function logBejelentkezes(Felhasznalo $felhasznalo) : void
    {
        $sql = 'CALL bejelentkezesLOG(:log_felhasznalo_id)';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

        $stmt->execute([
            'log_felhasznalo_id' => $felhasznalo->getFelhasznaloId()
        ]);
    }
}

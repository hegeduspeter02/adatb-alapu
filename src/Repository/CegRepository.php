<?php

namespace App\Repository;

use App\Entity\Ceg;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ceg>
 *
 * @method Ceg|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ceg|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ceg[]    findAll()
 * @method Ceg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CegRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ceg::class);
    }

    public function updateCeg(Ceg $ceg, int $ceg_id): void
    {
        $qb = $this->createQueryBuilder('c')
            ->update(Ceg::class, 'c')
            ->set('c.nev', ':nev')
            ->set('c.orszag', ':orszag')
            ->set('c.varos', ':varos')
            ->set('c.iranyitoszam', ':iranyitoszam')
            ->set('c.cim', ':cim')
            ->set('c.adoszam', ':adoszam')
            ->where('c.ceg_id = :ceg_id')
            ->setParameter('ceg_id', $ceg_id)
            ->setParameter('nev', $ceg->getNev())
            ->setParameter('orszag', $ceg->getOrszag())
            ->setParameter('varos', $ceg->getVaros())
            ->setParameter('iranyitoszam', $ceg->getIranyitoszam())
            ->setParameter('cim', $ceg->getCim())
            ->setParameter('adoszam', $ceg->getAdoszam());

        $query = $qb->getQuery();
        $query->execute();
    }

    public function deleteCeg(int $ceg_id): void
    {
        $qb = $this->createQueryBuilder('c')
            ->delete(Ceg::class, 'c')
            ->where('c.ceg_id = :ceg_id')
            ->setParameter('ceg_id', $ceg_id);

        $query = $qb->getQuery();
        $query->execute();
    }

//    /**
//     * @return Ceg[] Returns an array of Ceg objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Ceg
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

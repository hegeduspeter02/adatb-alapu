<?php

namespace App\Repository;

use App\Entity\TanulmanyiStatusz;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TanulmanyiStatusz>
 *
 * @method TanulmanyiStatusz|null find($id, $lockMode = null, $lockVersion = null)
 * @method TanulmanyiStatusz|null findOneBy(array $criteria, array $orderBy = null)
 * @method TanulmanyiStatusz[]    findAll()
 * @method TanulmanyiStatusz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TanulmanyiStatuszRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TanulmanyiStatusz::class);
    }

//    /**
//     * @return TanulmanyiStatusz[] Returns an array of TanulmanyiStatusz objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TanulmanyiStatusz
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\Pozicio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Pozicio>
 *
 * @method Pozicio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pozicio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pozicio[]    findAll()
 * @method Pozicio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PozicioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pozicio::class);
    }

    public function updatePozicio(Pozicio $pozicio, int $pozicio_id): void
    {
        $qb = $this->createQueryBuilder('c')
            ->update(Pozicio::class, 'c')
            ->set('c.pozicio_id', ':pozicio_id')
            ->set('c.munkaTipusa', ':munka_tipus_id')
            ->set('c.feladatkor', ':feladatkor')
            ->set('c.leiras', ':leiras')
            ->set('c.elvaras', ':elvaras')
            ->set('c.kinalat', ':kinalat')
            ->where('c.pozicio_id = :pozicio_id')
            ->setParameter('pozicio_id', $pozicio_id)
            ->setParameter('munka_tipus_id', $pozicio->getMunkaTipusa()->getMunkaTipusId())
            ->setParameter('feladatkor', $pozicio->getFeladatkor())
            ->setParameter('leiras', $pozicio->getLeiras())
            ->setParameter('elvaras', $pozicio->getElvaras())
            ->setParameter('kinalat', $pozicio->getKinalat());

        $query = $qb->getQuery();
        $query->execute();
    }

    public function deletePozicio(int $pozicio_id): void
    {
        $qb = $this->createQueryBuilder('c')
            ->delete(Pozicio::class, 'c')
            ->where('c.pozicio_id = :pozicio_id')
            ->setParameter('pozicio_id', $pozicio_id);

        $query = $qb->getQuery();
        $query->execute();
    }

    /**
     * @throws Exception
     */
    public function getApplicationsPerJobTypeAndResult(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT p.feladatkor, j.elutasitva, COUNT(*) AS jelentkezesek_szama
            FROM POZICIO p
            JOIN JELENTKEZES j ON j.pozicio_id = p.pozicio_id
            GROUP BY j.elutasitva, p.feladatkor
            ORDER BY jelentkezesek_szama DESC
        ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }

    public function getApplicationsPerJobType(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT p.feladatkor, COUNT(*) AS jelentkezesek_szama
            FROM POZICIO p
            JOIN JELENTKEZES j ON j.pozicio_id = p.pozicio_id
            GROUP BY p.feladatkor
            ORDER BY jelentkezesek_szama DESC
        ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }

    public function countJobTypePerJob(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT p.feladatkor, m_t.MEGNEVEZES, COUNT(*) AS munkak_szama
            FROM POZICIO p
            JOIN MUNKA_TIPUSA m_t ON m_t.MUNKA_TIPUS_ID = p.MUNKA_TIPUS_ID
            GROUP BY m_t.MEGNEVEZES, p.feladatkor
            ORDER BY p.feladatkor, munkak_szama DESC
        ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }

    public function getPositionsByCompany(): array
    {
        $conn=$this->getEntityManager()->getConnection();
        $sql="SELECT c.NEV, COUNT(*) AS db
              FROM POZICIO p, CEG c
              WHERE c.CEG_ID = p.CEG_ID
              GROUP BY c.NEV
              ORDER BY db";

        $res=$conn->executeQuery($sql);
        return $res->fetchAllAssociative();
    }



//    /**
//     * @return Pozicio[] Returns an array of Pozicio objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Pozicio
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

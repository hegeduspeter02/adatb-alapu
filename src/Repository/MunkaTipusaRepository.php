<?php

namespace App\Repository;

use App\Entity\MunkaTipusa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MunkaTipusa>
 *
 * @method MunkaTipusa|null find($id, $lockMode = null, $lockVersion = null)
 * @method MunkaTipusa|null findOneBy(array $criteria, array $orderBy = null)
 * @method MunkaTipusa[]    findAll()
 * @method MunkaTipusa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MunkaTipusaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MunkaTipusa::class);
    }

//    /**
//     * @return MunkaTipusa[] Returns an array of MunkaTipusa objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?MunkaTipusa
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

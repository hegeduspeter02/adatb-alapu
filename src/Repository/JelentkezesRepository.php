<?php

namespace App\Repository;

use App\Entity\Jelentkezes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Jelentkezes>
 *
 * @method Jelentkezes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jelentkezes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jelentkezes[]    findAll()
 * @method Jelentkezes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JelentkezesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Jelentkezes::class);
    }

    public function deleteJelentkezes(int $jelentkezes_id): void
    {
        $qb = $this->createQueryBuilder('j')
            ->delete(Jelentkezes::class, 'j')
            ->where('j.jelentkezes_id = :jelentkezes_id')
            ->setParameter('jelentkezes_id', $jelentkezes_id);

        $query = $qb->getQuery();
        $query->execute();
    }

    public function getApplicationsPerUser(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT FELHASZNALO.NEV, COUNT(*) as JELENTKEZESEK_SZAMA
            FROM FELHASZNALO, JELENTKEZES
            WHERE FELHASZNALO.FELHASZNALO_ID = JELENTKEZES.FELHASZNALO_ID
            GROUP BY FELHASZNALO.NEV
        ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }

//    /**
//     * @return Jelentkezes[] Returns an array of Jelentkezes objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('j')
//            ->andWhere('j.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('j.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Jelentkezes
//    {
//        return $this->createQueryBuilder('j')
//            ->andWhere('j.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\Ertekeles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Ertekeles>
 *
 * @method Ertekeles|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ertekeles|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ertekeles[]    findAll()
 * @method Ertekeles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ErtekelesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ertekeles::class);
    }

    public function getReviewAveragesPerCompany(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT CEG.NEV, AVG(ERTEKELES.PONTSZAM) AS ertekelesek_atlaga
            FROM CEG, ERTEKELES
            WHERE CEG.CEG_ID = ERTEKELES.CEG_ID
            GROUP BY CEG.NEV
        ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }

    public function addErtekelesWithDBProcedure(Ertekeles $ertekeles) : void
    {
        $sql = 'CALL ujertekeles(:ertekeles_id, :felhasznalo_id, :ceg_id, :pontszam, :szoveg)';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

        $stmt->execute([
            'ertekeles_id' => $ertekeles->getErtekelesId(),
            'felhasznalo_id' => $ertekeles->getFelhasznalo()->getFelhasznaloId(),
            'ceg_id' => $ertekeles->getCeg()->getCegId(),
            'pontszam' => $ertekeles->getPontszam(),
            'szoveg' => $ertekeles->getSzoveg()
        ]);
    }


//    /**
//     * @return Ertekeles[] Returns an array of Ertekeles objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Ertekeles
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

<?php

namespace App\Repository;

use App\Entity\Allaskereso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Allaskereso>
 *
 * @method Allaskereso|null find($id, $lockMode = null, $lockVersion = null)
 * @method Allaskereso|null findOneBy(array $criteria, array $orderBy = null)
 * @method Allaskereso[]    findAll()
 * @method Allaskereso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AllaskeresoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Allaskereso::class);
    }

    public function getNumberOfPeopleByStudentStatus():array
    {
        $conn=$this->getEntityManager()->getConnection();

        $sql="SELECT COUNT(*) AS DB, tanulmanyi_statusz.megnevezes as megnevezes
              FROM ALLASKERESO a
              join TANULMANYI_STATUSZ ON a.TAN_STATUSZ_ID = tanulmanyi_statusz.tan_statusz_id
              group by megnevezes";

        $result=$conn->executeQuery($sql);
        return $result->fetchAllAssociative();
    }
    public function getNumberOfPeopleLookingForJobs(){
        $conn=$this->getEntityManager()->getConnection();
        $sql="SELECT 
                (SELECT COUNT(*) FROM ALLASKERESO) AS Osszes,
                (SELECT COUNT(*) FROM ALLASKERESO) - (SELECT COUNT(DISTINCT FELHASZNALO_ID) FROM JELENTKEZES) AS Nemkereso,
                 (SELECT COUNT(DISTINCT FELHASZNALO_ID) FROM JELENTKEZES) AS Kereso
                FROM dual
            ";
        $result=$conn->executeQuery($sql);
        return $result->fetchAllAssociative();
            }

//    /**
//     * @return Allaskereso[] Returns an array of Allaskereso objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Allaskereso
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

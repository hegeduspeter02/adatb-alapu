<?php

namespace App\Repository;

use App\Entity\Felhasznalo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Felhasznalo>
 *
 * @method Felhasznalo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Felhasznalo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Felhasznalo[]    findAll()
 * @method Felhasznalo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FelhasznaloRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Felhasznalo::class);
    }

//    /**
//     * @return Felhasznalo[] Returns an array of Felhasznalo objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Felhasznalo
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

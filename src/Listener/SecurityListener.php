<?php

namespace App\Listener;
use App\Entity\LoginLogs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class SecurityListener
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if ($event->getAuthenticationToken()) {
            $user = $event->getAuthenticationToken()->getUser();
            if ($user) {
                $logRepository = $this->em->getRepository(LoginLogs::class);
                $logRepository->logBejelentkezes($user);
            }
        }

        /**
         * Update lastLogin and loginCount for user in database
         */

//  $em->

    }
}
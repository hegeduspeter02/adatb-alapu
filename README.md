# JobHub - Adatbázis alapú rendszerek project 2024

#### SB Admin 2 template: https://startbootstrap.com/theme/sb-admin-2

## Symfony és ORACLE setup lépések

### Lokális adatbázisban felhasználó létrehozása(ha még nem hoztál létre)
1. https://www.youtube.com/watch?v=LCC1Bli4ASY alapján
2. grant all privileges to letrehozott_user_neve;

### Adatbázis listener indítása
- minden windows insítás után meg kell csinálni
- cmd -> futtatás rendszergazdaként -> lsnrctl start

### Symfony telepítése

1. XAMPP telepítése(ha még nincs fent)
2. PHP 8.1.0 letöltése - ez a PHP verzió kell az oci8(database engine) működéséhez:
https://windows.php.net/downloads/releases/archives/php-8.1.0-Win32-vs16-x64.zip
    - majd kicsomagolni a C:\xampp\php81 mappába
    - PATH környezeti rendszerváltozó változó beállítása - írjátok felül az XAMPP által telepítettet(C:\xampp\php)
3. Composer telepítése: https://getcomposer.org/Composer-Setup.exe
4. Scoop telepítése(Windows PowerShell-ben):
   - "Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser"
   - "Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression"
5. Symfony CLI telepítése(PowerShell-ben): scoop install symfony-cli

### Oracle adatbázis függőségek telepítése
1. ORACLE Instant Client 12 telepítése(weboldalról bejelentkezés kell ezért felraktam MEGA-ra):
https://mega.nz/file/Rk1H3ZxD#UiYCOZ1leocCgxCUfDEwNvOX7WIY-C36afdTEHFeX9E
   - kicsomagolni a C:\oracle_instant_client mappába
   - Path rendszerváltozókhoz hozzáadása: C:\oracle_instant_client
2. oci8 database engine letöltése: https://pecl.php.net/package/oci8/3.2.1/windows
   - Thread Safe x64 verzió
   - kicsomagolása(mindent) a C:\xampp\php81\ext mappába
3. "C:\xampp\php81\php.ini-development" másolás és beillesztés ugyan oda(copy jön létre),
a copy-t nevezd át: php.ini-re
4. php.ini módosítása:
   - extension_dir = "ext"
   - Az extensions rész nézzen ki így:
       - extension=bz2
       - extension=curl
       - ;extension=ffi
       - extension=ftp
       - extension=fileinfo
       - extension=gd
       - extension=gettext
       - ;extension=gmp
       - extension=intl
       - ;extension=imap
       - ;extension=ldap
       - extension=mbstring
       - extension=exif      ; Must be after mbstring as it depends on it
       - extension=mysqli
       - extension=oci8_12c  ; Use with Oracle Database 12c Instant Client
       - ;extension=oci8_19  ; Use with Oracle Database 19 Instant Client
       - ;extension=odbc
       - extension=openssl
       - ;extension=pdo_firebird
       - extension=pdo_mysql
       - extension=pdo_oci
       - ;extension=pdo_odbc
       - ;extension=pdo_pgsql
       - extension=pdo_sqlite
       - ;extension=pgsql
       - ;extension=shmop
6. php 8.1 beállítása a project mappához
   - C:\xampp\apache\conf\extra\httpd-xampp.conf
   - Fájl aljára hozzáadni:
```
# PHP 8.1.0 Set-up
ScriptAlias /php81/ "C:/xampp/php81/"
Action application/x-httpd-php81-cgi "/php81/php-cgi.exe"
<Directory "C:/xampp/php81">
AllowOverride None
Options None
Require all denied
<Files "php-cgi.exe">
Require all granted
</Files>

    SetEnv PHPRC "C:/xampp/php81"
</Directory>

# adatb_alapu project with 8.1.0 php
# a directory a sajat munka konyvtarad
<Directory "C:/Users/hegip/Desktop/Egyetem/6. felev/adatb-alapu/adatb-alapu">
UnsetEnv PHPRC
<FilesMatch "\.php$">
php_flag engine off
SetHandler application/x-httpd-php81-cgi
</FilesMatch>
</Directory>
```

### Git telepítése a GIT Bash használatához
1. Letölt, telepít: https://git-scm.com/downloads
2. SSH kulcs gemerálása: https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab
3. Repo leklónnozása: git clone git@gitlab.com:hegeduspeter02/adatb-alapu.git

### Symfony setup
1. .env.local fájl létrehozása a project gyökerében, a DATABASE_URL kimásolása a .env-ből 
majd beillesztése a .env.local fájlba
2. az adatbázis felhasználójának nevét, jelszavát kell átírni, arra amit az sqldeveloperben létrehoztál
   - a példában a név a "C%23%23hegedusp"(#-et escape-elni kell %23-al)
   - a példában a jelszó az "admin"
3. composer install
4. npm install(ha van package.json)
5. lokális webszerver inítása
   - FONTOS! Ne használjátok az IDE parancssorát, használjatok Git Bash-t
   - parancs: "symfony server:start", localhost:8000-en elérhető

### Webpack parancsok js, css fájlok build-eléséhez
- npm run dev -> egyszer buildel
- npm run watch -> figyeli a fájlokat és ha változik akkor buildel
- npm run build -> végleges production buil(nem kell használni)
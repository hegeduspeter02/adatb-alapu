document.addEventListener('DOMContentLoaded', function() {
    var ctx = document.getElementById("myPieChart");

    var labels = [];
    var data = [];

    for (let i = 0; i < jobStatsJson.length; i++) {
        labels.push(jobStatsJson[i].FELADATKOR);
        data.push(parseInt(jobStatsJson[i].JELENTKEZESEK_SZAMA)); // Convert string to integer
    }

    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                backgroundColor: '#1cc88a',
                hoverBackgroundColor: '#2e59d9',
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });

});
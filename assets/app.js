// js files
import './bootstrap.js';
import $ from 'jquery';
import 'bootstrap';
import 'jquery.easing';
import 'datatables.net-bs4';
import 'datatables.net';
import 'chart.js';
import './js/sbadmin/sb-admin-2.min';
import './js/datatables';
import './js/area-chart';

// css files
import './styles/app.css';
import './styles/scss/sb-admin-2.scss';
import '@fortawesome/fontawesome-free/css/all.css';
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
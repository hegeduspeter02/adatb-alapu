<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240330210738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'ONLY FOR TESTING, DONT EXECUTE THIS MIGRATION!';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE ISEQ$$_79102');
        $this->addSql('DROP SEQUENCE ISEQ$$_79107');
        $this->addSql('DROP SEQUENCE ISEQ$$_79134');
        $this->addSql('DROP SEQUENCE ISEQ$$_79139');
        $this->addSql('DROP SEQUENCE ISEQ$$_79142');
        $this->addSql('DROP SEQUENCE ISEQ$$_79147');
        $this->addSql('DROP SEQUENCE ISEQ$$_79150');
        $this->addSql('DROP SEQUENCE ISEQ$$_79153');
        $this->addSql('DROP SEQUENCE ISEQ$$_79156');
        $this->addSql('DROP SEQUENCE ISEQ$$_79159');
        $this->addSql('CREATE SEQUENCE allaskereso_felhasznalo_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ceg_ceg_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ertekeles_ertekeles_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE jelentkezes_jelentkezes_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE felhasznalo_id_seq START WITH 10000 MINVALUE 10000 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE munka_tipusa_munka_tipus_id_se START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE pozicio_pozicio_id_seq START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE tanulmanyi_statusz_tan_statusz START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('ALTER TABLE ALLASKERESO DROP CONSTRAINT SYS_C0010015');
        $this->addSql('ALTER TABLE ALLASKERESO MODIFY (felhasznalo_id NUMBER(10) DEFAULT NULL, jogosultsag CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE ALLASKERESO ADD CONSTRAINT FK_2F44344D1770725E FOREIGN KEY (tan_statusz_id) REFERENCES tanulmanyi_statusz (tan_statusz_id)');
        $this->addSql('ALTER INDEX idx_a247a6a54289c154 RENAME TO IDX_2F44344D1770725E');
        $this->addSql('ALTER TABLE CEG DROP CONSTRAINT SYS_C0010024');
        $this->addSql('ALTER TABLE CEG MODIFY (ceg_id NUMBER(10) DEFAULT NULL)');
        $this->addSql('ALTER TABLE CEG ADD CONSTRAINT FK_7E8CC77250A159B FOREIGN KEY (felhasznalo_id) REFERENCES felhasznalo (felhasznalo_id)');
        $this->addSql('ALTER INDEX idx_e82b85f8d66f2946 RENAME TO IDX_7E8CC77250A159B');
        $this->addSql('ALTER TABLE ERTEKELES DROP CONSTRAINT SYS_C0010047');
        $this->addSql('ALTER TABLE ERTEKELES DROP CONSTRAINT SYS_C0010048');
        $this->addSql('ALTER TABLE ERTEKELES MODIFY (ertekeles_id NUMBER(10) DEFAULT NULL)');
        $this->addSql('ALTER TABLE ERTEKELES ADD CONSTRAINT FK_2CC7E03450A159B FOREIGN KEY (felhasznalo_id) REFERENCES felhasznalo (felhasznalo_id)');
        $this->addSql('ALTER TABLE ERTEKELES ADD CONSTRAINT FK_2CC7E03499347694 FOREIGN KEY (CEG_ID) REFERENCES ceg (ceg_id)');
        $this->addSql('ALTER INDEX idx_6805fedbd66f2946 RENAME TO IDX_2CC7E03450A159B');
        $this->addSql('ALTER INDEX idx_6805fedb99347694 RENAME TO IDX_2CC7E03499347694');
        $this->addSql('ALTER TABLE FELHASZNALO MODIFY (jogosultsag CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE JELENTKEZES DROP CONSTRAINT SYS_C0010040');
        $this->addSql('ALTER TABLE JELENTKEZES DROP CONSTRAINT SYS_C0010041');
        $this->addSql('ALTER TABLE JELENTKEZES MODIFY (jelentkezes_id NUMBER(10) DEFAULT NULL)');
        $this->addSql('ALTER TABLE JELENTKEZES ADD CONSTRAINT FK_EDAE07EB50A159B FOREIGN KEY (felhasznalo_id) REFERENCES felhasznalo (felhasznalo_id)');
        $this->addSql('ALTER TABLE JELENTKEZES ADD CONSTRAINT FK_EDAE07EB55A0B44A FOREIGN KEY (pozicio_id) REFERENCES pozicio (pozicio_id)');
        $this->addSql('ALTER INDEX idx_60ad9503d66f2946 RENAME TO IDX_EDAE07EB50A159B');
        $this->addSql('ALTER INDEX idx_60ad95036672ef95 RENAME TO IDX_EDAE07EB55A0B44A');
        $this->addSql('ALTER TABLE KAPCSOLATTARTO MODIFY (felhasznalo_id NUMBER(10) DEFAULT NULL, jogosultsag CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE MUNKA_TIPUSA MODIFY (munka_tipus_id NUMBER(10) DEFAULT NULL)');
        $this->addSql('ALTER TABLE POZICIO DROP CONSTRAINT SYS_C0010034');
        $this->addSql('ALTER TABLE POZICIO DROP CONSTRAINT SYS_C0010035');
        $this->addSql('ALTER TABLE POZICIO MODIFY (pozicio_id NUMBER(10) DEFAULT NULL)');
        $this->addSql('ALTER TABLE POZICIO ADD CONSTRAINT FK_5E492B6380B1E1D3 FOREIGN KEY (munka_tipus_id) REFERENCES munka_tipusa (munka_tipus_id)');
        $this->addSql('ALTER TABLE POZICIO ADD CONSTRAINT FK_5E492B635181F7F2 FOREIGN KEY (ceg_id) REFERENCES ceg (ceg_id)');
        $this->addSql('ALTER INDEX idx_610cd579d1255fad RENAME TO IDX_5E492B6380B1E1D3');
        $this->addSql('ALTER INDEX idx_610cd57999347694 RENAME TO IDX_5E492B635181F7F2');
        $this->addSql('ALTER TABLE TANULMANYI_STATUSZ MODIFY (tan_statusz_id NUMBER(10) DEFAULT NULL)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE allaskereso_felhasznalo_id_seq');
        $this->addSql('DROP SEQUENCE ceg_ceg_id_seq');
        $this->addSql('DROP SEQUENCE ertekeles_ertekeles_id_seq');
        $this->addSql('DROP SEQUENCE jelentkezes_jelentkezes_id_seq');
        $this->addSql('DROP SEQUENCE felhasznalo_id_seq');
        $this->addSql('DROP SEQUENCE munka_tipusa_munka_tipus_id_se');
        $this->addSql('DROP SEQUENCE pozicio_pozicio_id_seq');
        $this->addSql('DROP SEQUENCE tanulmanyi_statusz_tan_statusz');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79102 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79107 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79134 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79139 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79142 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79147 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79150 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79153 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79156 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('CREATE SEQUENCE ISEQ$$_79159 START WITH 1 MINVALUE 1 INCREMENT BY 1');
        $this->addSql('ALTER TABLE felhasznalo MODIFY (JOGOSULTSAG CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE tanulmanyi_statusz MODIFY (TAN_STATUSZ_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79139".nextval)');
        $this->addSql('ALTER TABLE allaskereso DROP CONSTRAINT FK_2F44344D1770725E');
        $this->addSql('ALTER TABLE allaskereso MODIFY (FELHASZNALO_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79142".nextval, JOGOSULTSAG CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE allaskereso ADD CONSTRAINT SYS_C0010015 FOREIGN KEY (TAN_STATUSZ_ID) REFERENCES TANULMANYI_STATUSZ (TAN_STATUSZ_ID) ON DELETE SET NULL');
        $this->addSql('ALTER INDEX idx_2f44344d1770725e RENAME TO IDX_A247A6A54289C154');
        $this->addSql('ALTER TABLE ceg DROP CONSTRAINT FK_7E8CC77250A159B');
        $this->addSql('ALTER TABLE ceg MODIFY (CEG_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79147".nextval)');
        $this->addSql('ALTER TABLE ceg ADD CONSTRAINT SYS_C0010024 FOREIGN KEY (FELHASZNALO_ID) REFERENCES KAPCSOLATTARTO (FELHASZNALO_ID) ON DELETE SET NULL');
        $this->addSql('ALTER INDEX idx_7e8cc77250a159b RENAME TO IDX_E82B85F8D66F2946');
        $this->addSql('ALTER TABLE kapcsolattarto MODIFY (FELHASZNALO_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79134".nextval, JOGOSULTSAG CLOB DEFAULT NULL)');
        $this->addSql('ALTER TABLE ertekeles DROP CONSTRAINT FK_2CC7E03450A159B');
        $this->addSql('ALTER TABLE ertekeles DROP CONSTRAINT FK_2CC7E03499347694');
        $this->addSql('ALTER TABLE ertekeles MODIFY (ERTEKELES_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79159".nextval)');
        $this->addSql('ALTER TABLE ertekeles ADD CONSTRAINT SYS_C0010047 FOREIGN KEY (FELHASZNALO_ID) REFERENCES FELHASZNALO (FELHASZNALO_ID) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ertekeles ADD CONSTRAINT SYS_C0010048 FOREIGN KEY (CEG_ID) REFERENCES CEG (CEG_ID) ON DELETE CASCADE');
        $this->addSql('ALTER INDEX idx_2cc7e03450a159b RENAME TO IDX_6805FEDBD66F2946');
        $this->addSql('ALTER INDEX idx_2cc7e03499347694 RENAME TO IDX_6805FEDB99347694');
        $this->addSql('ALTER TABLE pozicio DROP CONSTRAINT FK_5E492B6380B1E1D3');
        $this->addSql('ALTER TABLE pozicio DROP CONSTRAINT FK_5E492B635181F7F2');
        $this->addSql('ALTER TABLE pozicio MODIFY (POZICIO_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79153".nextval)');
        $this->addSql('ALTER TABLE pozicio ADD CONSTRAINT SYS_C0010034 FOREIGN KEY (MUNKA_TIPUS_ID) REFERENCES MUNKA_TIPUSA (MUNKA_TIPUS_ID) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE pozicio ADD CONSTRAINT SYS_C0010035 FOREIGN KEY (CEG_ID) REFERENCES CEG (CEG_ID) ON DELETE CASCADE');
        $this->addSql('ALTER INDEX idx_5e492b6380b1e1d3 RENAME TO IDX_610CD579D1255FAD');
        $this->addSql('ALTER INDEX idx_5e492b635181f7f2 RENAME TO IDX_610CD57999347694');
        $this->addSql('ALTER TABLE munka_tipusa MODIFY (MUNKA_TIPUS_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79150".nextval)');
        $this->addSql('ALTER TABLE jelentkezes DROP CONSTRAINT FK_EDAE07EB50A159B');
        $this->addSql('ALTER TABLE jelentkezes DROP CONSTRAINT FK_EDAE07EB55A0B44A');
        $this->addSql('ALTER TABLE jelentkezes MODIFY (JELENTKEZES_ID NUMBER(10) DEFAULT "C##HEGEDUSP"."ISEQ$$_79156".nextval)');
        $this->addSql('ALTER TABLE jelentkezes ADD CONSTRAINT SYS_C0010040 FOREIGN KEY (FELHASZNALO_ID) REFERENCES ALLASKERESO (FELHASZNALO_ID) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jelentkezes ADD CONSTRAINT SYS_C0010041 FOREIGN KEY (POZICIO_ID) REFERENCES POZICIO (POZICIO_ID) ON DELETE CASCADE');
        $this->addSql('ALTER INDEX idx_edae07eb50a159b RENAME TO IDX_60AD9503D66F2946');
        $this->addSql('ALTER INDEX idx_edae07eb55a0b44a RENAME TO IDX_60AD95036672EF95');
    }
}

DROP TABLE LOGIN_LOGS;
DROP TABLE ERTEKELES;
DROP TABLE JELENTKEZES;
DROP TABLE POZICIO;
DROP TABLE MUNKA_TIPUSA;
DROP TABLE CEG;
DROP TABLE ALLASKERESO;
DROP TABLE TANULMANYI_STATUSZ;
DROP TABLE KAPCSOLATTARTO;
DROP TABLE FELHASZNALO;
DROP TABLE POZICIO_LOGS;

-- FELHASZNALO ŐS
CREATE TABLE FELHASZNALO
(
    FELHASZNALO_ID INT,
    NEV            VARCHAR2(50) NOT NULL,
    JELSZO         VARCHAR2(255) NOT NULL, -- hashelt
    EMAIL          VARCHAR2(50) NOT NULL,
    JOGOSULTSAG    CLOB NOT NULL,
    PRIMARY KEY (FELHASZNALO_ID)
);

-- FELHASZNALO-i adatok letárolása + MUNKAHELY + BEOSZTAS
CREATE TABLE KAPCSOLATTARTO
(
    FELHASZNALO_ID INT  NOT NULL,
    NEV            VARCHAR2(50) NOT NULL,
    JELSZO         VARCHAR2(255) NOT NULL,
    EMAIL          VARCHAR2(50) NOT NULL,
    JOGOSULTSAG    CLOB NOT NULL,
    BEOSZTAS       VARCHAR2(30) NOT NULL,
    PRIMARY KEY (FELHASZNALO_ID)
);

-- ALLASKERESO tanulmányi státusza
CREATE TABLE TANULMANYI_STATUSZ
(
    TAN_STATUSZ_ID INT GENERATED ALWAYS AS IDENTITY,
    MEGNEVEZES     VARCHAR2(30) NOT NULL,
    PRIMARY KEY (TAN_STATUSZ_ID)
);

-- FELHASZNALO-i adatok letárolása + álláskeresőként szükséges adatok
CREATE TABLE ALLASKERESO
(
    FELHASZNALO_ID INT  NOT NULL,
    TAN_STATUSZ_ID INT,
    NEV            VARCHAR2(50) NOT NULL,
    JELSZO         VARCHAR2(255) NOT NULL,
    EMAIL          VARCHAR2(50) NOT NULL,
    JOGOSULTSAG    CLOB NOT NULL,
    VAROS          VARCHAR2(20) NOT NULL,
    TELEFONSZAM    VARCHAR2(15),
    ONELETRAJZ     VARCHAR2(150), -- hivatkozás
    PRIMARY KEY (FELHASZNALO_ID),
    FOREIGN KEY (TAN_STATUSZ_ID)
        REFERENCES TANULMANYI_STATUSZ (TAN_STATUSZ_ID)
        ON DELETE SET NULL        -- megszűnik egy tanulmányi státusz, legyen NULL, de ne törlődjön az álláskereső
);

-- pozíciót hirdetni kívánó cég adatai
CREATE TABLE CEG
(
    CEG_ID         INT NOT NULL,
    FELHASZNALO_ID INT,
    NEV            VARCHAR2(50) NOT NULL,
    ORSZAG         VARCHAR2(20) NOT NULL,
    VAROS          VARCHAR2(20) NOT NULL,
    IRANYITOSZAM   VARCHAR2(10) NOT NULL,
    CIM            VARCHAR2(100) NOT NULL,
    ADOSZAM        NUMBER(11) NOT NULL,
    PRIMARY KEY (CEG_ID),
    FOREIGN KEY (FELHASZNALO_ID)
        REFERENCES KAPCSOLATTARTO (FELHASZNALO_ID)
        ON DELETE SET NULL -- törlődik a céges kapcsolattartó, legyen NULL, de ne törlődjön a cég
);

-- ilyen típusú munkát lehet hirdetni
CREATE TABLE MUNKA_TIPUSA
(
    MUNKA_TIPUS_ID INT GENERATED ALWAYS AS IDENTITY,
    MEGNEVEZES     VARCHAR2(30) NOT NULL, -- pl. részmunkaidős, szakmai gyakorlat, diákmunka
    PRIMARY KEY (MUNKA_TIPUS_ID)
);

-- cég által meghirdetett pozíció adatai
CREATE TABLE POZICIO
(
    POZICIO_ID     INT NOT NULL,
    MUNKA_TIPUS_ID INT,
    CEG_ID         INT NOT NULL,
    FELADATKOR     VARCHAR2(30) NOT NULL,  -- pl. programozó
    LEIRAS         VARCHAR2(255),          -- pl. A feladat SAP rendszerek karbantartása.
    ELVARAS        VARCHAR2(255) NOT NULL, -- pl. C# ismeret
    KINALAT        VARCHAR2(255),          -- pl. barátságos kollegák
    PRIMARY KEY (POZICIO_ID),
    FOREIGN KEY (MUNKA_TIPUS_ID)
        REFERENCES MUNKA_TIPUSA (MUNKA_TIPUS_ID)
        ON DELETE SET NULL,                -- törlődik pl. a diákmunka típus, de maradjanak meg az eddig meghirdetett diákmunkák
    FOREIGN KEY (CEG_ID)
        REFERENCES CEG (CEG_ID)
        ON DELETE CASCADE                  -- törlődik cég, törlődjönek az általa meghirdetett pozíciók is
);

-- álláskeresői jelentkezések a cég által hirdetett pozíciókra
CREATE TABLE JELENTKEZES
(
    JELENTKEZES_ID INT NOT NULL,
    FELHASZNALO_ID INT NOT NULL,
    POZICIO_ID     INT NOT NULL,
    UZENET         VARCHAR2(255),
    ELUTASITVA     INT,
    PRIMARY KEY (JELENTKEZES_ID),
    FOREIGN KEY (FELHASZNALO_ID)
        REFERENCES ALLASKERESO (FELHASZNALO_ID)
        ON DELETE CASCADE, -- törlődik az álláskereső, az általa beadott jelentkezések is törlődjenek
    FOREIGN KEY (POZICIO_ID)
        REFERENCES POZICIO (POZICIO_ID)
        ON DELETE CASCADE  -- törlődik egy pozíció, törlődjenek az oda szóló jelentkezések is
);

-- felhasználói értékelések a cégekről
CREATE TABLE ERTEKELES
(
    ERTEKELES_ID   INT NOT NULL,
    FELHASZNALO_ID INT,
    CEG_ID         INT NOT NULL,
    PONTSZAM       NUMBER(2) NOT NULL, -- 0-10-ig értékel pontszáma
    SZOVEG         VARCHAR2(255),
    PRIMARY KEY (ERTEKELES_ID),
    FOREIGN KEY (FELHASZNALO_ID)
        REFERENCES FELHASZNALO (FELHASZNALO_ID)
        ON DELETE SET NULL,            -- felhasználó törlődik, de az általa írt értékelések ne törlődjenek
    FOREIGN KEY (CEG_ID)
        REFERENCES CEG (CEG_ID)
        ON DELETE CASCADE              -- törlődik a cég, törlődjenek az ide szóló értékelések is
);

-- a poziciók tábla módosításainak nyilvántartása
create table POZICIO_LOGS
(
    LOG_ID              INTEGER generated as identity
        constraint "POZICIO_LOGS_pk"
            primary key,
    CEG_ID              INTEGER   not null,
    MUNKA_TIPUSA_ID_OLD INTEGER   not null,
    MUNKA_TIPUSA_ID_NEW INTEGER   not null,
    FELADATKOR_OLD      VARCHAR2(30) not null,
    FELADATKOR_NEW      VARCHAR2(30) not null,
    LEIRAS_OLD          VARCHAR2(255),
    LEIRAS_NEW          VARCHAR2(255),
    ELVARAS_OLD         VARCHAR2(255) not null,
    ELVARAS_NEW         VARCHAR2(255) not null,
    KINALAT_OLD         VARCHAR2(255),
    KINALAT_NEW         VARCHAR2(255),
    EDIT_TIME           TIMESTAMP not null
);

-- a bejelentkezések nyilvántartása
create table LOGIN_LOGS
(
    LOGIN_LOG_ID   INTEGER generated as identity
        constraint "LOGIN_LOGS_pk"
            primary key,
    FELHASZNALO_ID INTEGER   not null,
    LOGIN_TIME     TIMESTAMP not null,
    FOREIGN KEY (FELHASZNALO_ID)
        REFERENCES FELHASZNALO (FELHASZNALO_ID)
        ON DELETE CASCADE -- törlődik egy felhasználó, törlődnek a bejelentkezés logok is
);

-- trigger a poziciók tábla módosításainak nyilvántartásához
CREATE
OR REPLACE TRIGGER pozicio_edit_log_trigger
    AFTER
UPDATE OR
DELETE
ON POZICIO
    FOR EACH ROW
BEGIN
INSERT INTO POZICIO_LOGS(CEG_ID, MUNKA_TIPUSA_ID_OLD, MUNKA_TIPUSA_ID_NEW, FELADATKOR_OLD, FELADATKOR_NEW, LEIRAS_OLD,
                         LEIRAS_NEW, ELVARAS_OLD, ELVARAS_NEW, KINALAT_OLD, KINALAT_NEW, EDIT_TIME)
VALUES (:OLD.CEG_ID, :OLD.MUNKA_TIPUS_ID, :NEW.MUNKA_TIPUS_ID, :OLD.FELADATKOR, :NEW.FELADATKOR, :OLD.LEIRAS,
        :NEW.LEIRAS,
        :OLD.ELVARAS, :NEW.ELVARAS, :OLD.KINALAT, :NEW.KINALAT, SYSDATE);
END;
/

-- eljárás a bejelentkezések logolásához
CREATE
OR replace PROCEDURE bejelentkezesLOG (
    log_felhasznalo_id IN NUMBER)
AS
BEGIN
INSERT INTO LOGIN_LOGS(FELHASZNALO_ID, LOGIN_TIME)
VALUES (log_felhasznalo_id, SYSDATE);
END;
/

-- eljárás az értékelések pontszámának 1 és 10 között tartásához
CREATE
or REPLACE PROCEDURE UjErtekeles (
    ertekeles_id IN NUMBER,
    felhasznalo_id IN NUMBER,
    ceg_id IN NUMBER,
    pontszam IN NUMBER,
    szoveg IN VARCHAR2)
AS
BEGIN
    IF
pontszam < 1 THEN
        INSERT INTO ertekeles VALUES (ertekeles_id, felhasznalo_id, ceg_id, 1, szoveg);
    ELSIF
pontszam > 10 THEN
        INSERT INTO ertekeles VALUES (ertekeles_id, felhasznalo_id, ceg_id, 10, szoveg);
ELSE
        INSERT INTO ertekeles VALUES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg);
END IF;
END;
/

-- KAPCSOLATTARTÓ ADATOK FELTÖLTÉSE
INSERT INTO KAPCSOLATTARTO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, BEOSZTAS) VALUES (10001, 'Huszka Péter', '$2y$13$lJ9dnGx8v0WMFSk/m7KaDOR2ervH7.7KYYdQ3ke0bSdwt7VrSbl5C', 'huszi@gmail.com', '["ROLE_KAPCSOLATTARTO"]', 'HR');
INSERT INTO KAPCSOLATTARTO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, BEOSZTAS)
VALUES (10002, 'Ungi Zsiga', '$2y$13$V5dmLrpk7FQnLP//1pG.S.Zq647AmrdIlTnjJQbJ3icl2Gr7MNQ4m', 'zsigmond@ceg.com',
        '["ROLE_KAPCSOLATTARTO"]', 'Projekt manager');
INSERT INTO KAPCSOLATTARTO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, BEOSZTAS)
VALUES (10003, 'Vándor Benedek', '$2y$13$uYBRV3VXXcz9j05arOZoxuqn0b0jH5uRQ4NxOba37ztRQW67lDN8K', 'benedek@vallalat.hu',
        '["ROLE_KAPCSOLATTARTO"]', 'HR');
INSERT INTO KAPCSOLATTARTO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, BEOSZTAS)
VALUES (10004, 'Mennyhárt Kristóf', '$2y$13$dWJ0Zj28WrKtDtq0/x3Lfu5ifqsdKKuTCmrYGt1nKmHIjMcp17cPO', 'krist@gcc.com',
        '["ROLE_KAPCSOLATTARTO"]', 'gyakornok');
INSERT INTO KAPCSOLATTARTO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, BEOSZTAS)
VALUES (10005, 'Forró Ernő', '$2y$13$sGA6lcegq4LwQM5hIRBqvOhhpGSa1IcYuSKq0f11rHk9f3SXlblg6', 'ernold@ceges.com',
        '["ROLE_KAPCSOLATTARTO"]', 'CEO');


-- TANULMANYI_STATUSZ ADATOK FELTÖLTÉSE
INSERT INTO TANULMANYI_STATUSZ (MEGNEVEZES)
values ('közoktatásbeli diák');
INSERT INTO TANULMANYI_STATUSZ (MEGNEVEZES)
values ('egyetemista/főiskolás');
INSERT INTO TANULMANYI_STATUSZ (MEGNEVEZES)
values ('nem diák');


-- ÁLLÁSKERESŐ ADATOK FELTÖLTÉSE
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (1, 1, 'Sándor János', '$2y$13$AdizWcRq1AHSLJ3Z30ewvOM8hjoEl0OuQJI0G3uv7YzjjDM11IyXi', 'sanya@gmail.com',
        '["ROLE_ALLASKERESO"]', 'Szeged', null, 'N/A');
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (2, 2, 'Horváth Tivadar', '$2y$13$.jwg7XTqF6T/H4WT.xR2s.0pDqfoGILs4MkGOzRwM8axnFfTkWWFK', 'tiv@freemail.hu',
        '["ROLE_ALLASKERESO"]', 'Szeged', null, null);
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (3, 1, 'Vízhányó Dorka', '$2y$13$5LR3AZ6czE.D5Uxf7Th7ceOkHEd8x/vi1xxyVo4ZViaRYChmVErsu', 'dorci@pr.com',
        '["ROLE_ALLASKERESO"]', 'Makó', null, 'N/A');
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (4, 3, 'Szegedi Janka', '$2y$13$hGCYrfzax08Cb2NsWY/C9uNpAyCHOE3lxBDckQaZB9qn.Xfm5jAu2', 'janesz@gmail.com',
        '["ROLE_ALLASKERESO"]', 'Hódmezővásárhely', null, null);
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (5, 1, 'Vándor Gábor', '$2y$13$y9m6lzBVYy1Mxc67HZUxROw4zeQ3eXl2O4oQYH./x4LrGm2GWRSIi', 'gabi@gmail.com',
        '["ROLE_ALLASKERESO"]', 'Szeged', null, 'N/A');
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (6, 1, 'Hegedűs Péter', '$2y$13$o1PuKFdH9wSK5HKxc1kzqOhI7TY2.RGyF/hL3Mz8m5I3GPM1jT/j.', 'admin@admin.hu',
        '["ROLE_ADMIN"]', 'N/A', null, 'N/A');
INSERT INTO ALLASKERESO (FELHASZNALO_ID, TAN_STATUSZ_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG, VAROS, TELEFONSZAM,
                         ONELETRAJZ)
VALUES (7, 1, 'Klézili Richárd', '$2y$13$Ii21/nv1QTMcrLq2sDTA.O0//dPt9lw.GyrSo77UVOikNwHtawtW2', 'asd@admin.hu',
        '["ROLE_ADMIN"]', 'N/A', null, 'N/A');


-- FELHASZNÁLÓ ADATOK FELTÖLTÉSE
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (1, 'Sándor János', '$2y$13$AdizWcRq1AHSLJ3Z30ewvOM8hjoEl0OuQJI0G3uv7YzjjDM11IyXi', 'sanya@gmail.com',
        '["ROLE_ALLASKERESO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (2, 'Horváth Tivadar', '$2y$13$.jwg7XTqF6T/H4WT.xR2s.0pDqfoGILs4MkGOzRwM8axnFfTkWWFK', 'tiv@freemail.hu',
        '["ROLE_ALLASKERESO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (3, 'Vízhányó Dorka', '$2y$13$5LR3AZ6czE.D5Uxf7Th7ceOkHEd8x/vi1xxyVo4ZViaRYChmVErsu', 'dorci@pr.com',
        '["ROLE_ALLASKERESO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (4, 'Szegedi Janka', '$2y$13$hGCYrfzax08Cb2NsWY/C9uNpAyCHOE3lxBDckQaZB9qn.Xfm5jAu2', 'janesz@gmail.com',
        '["ROLE_ALLASKERESO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (5, 'Vándor Gábor', '$2y$13$y9m6lzBVYy1Mxc67HZUxROw4zeQ3eXl2O4oQYH./x4LrGm2GWRSIi', 'gabi@gmail.com',
        '["ROLE_ALLASKERESO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (6, 'Hegedűs Péter', '$2y$13$o1PuKFdH9wSK5HKxc1kzqOhI7TY2.RGyF/hL3Mz8m5I3GPM1jT/j.', 'admin@admin.hu',
        '["ROLE_ADMIN"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (7, 'Klézili Richárd', '$2y$13$Ii21/nv1QTMcrLq2sDTA.O0//dPt9lw.GyrSo77UVOikNwHtawtW2', 'asd@admin.hu',
        '["ROLE_ADMIN"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (10001, 'Huszka Péter', '$2y$13$lJ9dnGx8v0WMFSk/m7KaDOR2ervH7.7KYYdQ3ke0bSdwt7VrSbl5C', 'huszi@gmail.com',
        '["ROLE_KAPCSOLATTARTO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (10002, 'Ungi Zsiga', '$2y$13$V5dmLrpk7FQnLP//1pG.S.Zq647AmrdIlTnjJQbJ3icl2Gr7MNQ4m', 'zsigmond@ceg.com',
        '["ROLE_KAPCSOLATTARTO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (10003, 'Vándor Benedek', '$2y$13$uYBRV3VXXcz9j05arOZoxuqn0b0jH5uRQ4NxOba37ztRQW67lDN8K', 'benedek@vallalat.hu',
        '["ROLE_KAPCSOLATTARTO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (10004, 'Mennyhárt Kristóf', '$2y$13$dWJ0Zj28WrKtDtq0/x3Lfu5ifqsdKKuTCmrYGt1nKmHIjMcp17cPO', 'krist@gcc.com',
        '["ROLE_KAPCSOLATTARTO"]');
INSERT INTO FELHASZNALO (FELHASZNALO_ID, NEV, JELSZO, EMAIL, JOGOSULTSAG)
VALUES (10005, 'Forró Ernő', '$2y$13$sGA6lcegq4LwQM5hIRBqvOhhpGSa1IcYuSKq0f11rHk9f3SXlblg6', 'ernold@ceges.com',
        '["ROLE_KAPCSOLATTARTO"]');


-- CÉG ADATOK FELTÖLTÉSE
INSERT INTO CEG (CEG_ID, FELHASZNALO_ID, NEV, ORSZAG, VAROS, IRANYITOSZAM, CIM, ADOSZAM)
VALUES (1, 10001, 'Interspar', 'Magyarország', 'Szeged', '6700', 'Oskola utca 22.', 83439295800);
INSERT INTO CEG (CEG_ID, FELHASZNALO_ID, NEV, ORSZAG, VAROS, IRANYITOSZAM, CIM, ADOSZAM)
VALUES (2, 10002, 'SZTE', 'Magyarország', 'Szeged', '6700', 'Széchényi tér 2.', 25744596351);
INSERT INTO CEG (CEG_ID, FELHASZNALO_ID, NEV, ORSZAG, VAROS, IRANYITOSZAM, CIM, ADOSZAM)
VALUES (3, 10003, 'Sro Tatra', 'Szlovákia', 'Bratislava', '84105', 'Agátová ulica 4.', 85245965744);
INSERT INTO CEG (CEG_ID, FELHASZNALO_ID, NEV, ORSZAG, VAROS, IRANYITOSZAM, CIM, ADOSZAM)
VALUES (4, 10004, 'Budapest éjjel-nappal', 'Magyarország', 'Budapest', '9658', 'Akácfa utca 52.', 56238451289);
INSERT INTO CEG (CEG_ID, FELHASZNALO_ID, NEV, ORSZAG, VAROS, IRANYITOSZAM, CIM, ADOSZAM)
VALUES (5, 10005, 'TECH Világ Zrt.', 'Magyarország', 'Esztergom', '4582', 'Andrássy utca 45.', 85221455639);


-- MUNKA TÍPUSÁNAK FELTÖLTÉSE
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Teljes munkaidős');
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Részmunkaidős');
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Szakmai gyakorlat');
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Diákmunka');
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Szezonális munka');
INSERT INTO MUNKA_TIPUSA (MEGNEVEZES)
VALUES ('Projektalapú munka');


-- POZÍCIÓK FELTÖLTÉSE
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (1, 1, 1, 'Építőmérnök', 'ipsum primis in faucibus orci luctus et ultrices posuere cubilia',
        'vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt',
        'est, mollis non, cursus non, egestas a,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (2, 1, 2, 'Tudományos kutató',
        'Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas',
        'et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan',
        'vitae risus. Duis a mi fringilla mi');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (3, 2, 3, 'Építőmérnök', 'quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis.',
        'ridiculus mus. Donec dignissim magna a tortor. Nunc',
        'Phasellus fermentum convallis ligula. Donec luctus aliquet odio.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (4, 2, 4, 'Szoftverfejlesztő', 'cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis',
        'Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem',
        'eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (5, 3, 5, 'Tudományos kutató', 'ornare, lectus ante dictum mi, ac mattis velit justo nec',
        'aliquam iaculis, lacus pede sagittis augue,', 'Aliquam auctor, velit eget laoreet posuere, enim nisl');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (6, 3, 1, 'Humánerőforrás tanácsadó', 'ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac',
        'pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam',
        'ullamcorper, velit in aliquet lobortis, nisi nibh');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (7, 4, 2, 'Tanár', 'enim, sit amet ornare lectus justo eu arcu. Morbi sit',
        'a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras', 'eu, euismod ac, fermentum vel, mauris.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (8, 4, 3, 'Tudományos kutató', 'mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent',
        'auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget,',
        'Mauris eu turpis. Nulla aliquet. Proin');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (9, 5, 4, 'Szoftverfejlesztő', 'vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci',
        'nunc sit amet metus. Aliquam erat volutpat.', 'amet, consectetuer adipiscing elit. Etiam laoreet, libero');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (10, 5, 5, 'Értékesítési asszisztens', 'commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac,',
        'eu neque pellentesque massa lobortis ultrices. Vivamus', 'eu, ligula. Aenean euismod mauris eu elit. Nulla');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (11, 6, 1, 'Jogász', 'Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie',
        'rhoncus. Nullam velit dui, semper et, lacinia vitae,', 'dapibus gravida. Aliquam tincidunt, nunc');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (12, 6, 2, 'Szoftverfejlesztő', 'nisi. Cum sociis natoque penatibus et magnis dis parturient montes,',
        'nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem,',
        'primis in faucibus orci luctus et ultrices');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (13, 1, 3, 'Értékesítési asszisztens', 'ipsum porta elit, a feugiat tellus lorem eu metus. In',
        'commodo ipsum. Suspendisse non leo. Vivamus nibh dolor,', 'Nam nulla magna, malesuada vel, convallis in,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (14, 1, 4, 'Könyvelő', 'sagittis augue, eu tempor erat neque non quam. Pellentesque habitant',
        'sapien. Aenean massa. Integer vitae nibh. Donec est', 'elementum purus, accumsan interdum libero dui nec');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (15, 2, 5, 'Szoftverfejlesztő', 'pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus',
        'sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula',
        'sit amet, consectetuer adipiscing elit. Aliquam');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (16, 2, 1, 'Értékesítési asszisztens', 'Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci',
        'odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis',
        'est, vitae sodales nisi magna sed dui. Fusce');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (17, 3, 2, 'Tudományos kutató',
        'felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum.',
        'quis lectus. Nullam suscipit, est ac facilisis facilisis, magna',
        'diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (18, 3, 3, 'Jogász', 'quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus',
        'amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean',
        'at auctor ullamcorper, nisl arcu iaculis enim, sit');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (19, 4, 4, 'Értékesítési asszisztens', 'Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.',
        'id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum',
        'Duis risus odio, auctor vitae, aliquet nec, imperdiet');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (20, 4, 5, 'Építőmérnök', 'odio vel est tempor bibendum. Donec felis orci, adipiscing non,',
        'quis diam luctus lobortis. Class aptent taciti sociosqu',
        'posuere cubilia Curae Phasellus ornare. Fusce mollis. Duis sit amet');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (21, 5, 1, 'Szoftverfejlesztő', 'Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat.',
        'Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut',
        'iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (22, 5, 2, 'Értékesítési asszisztens', 'elit elit fermentum risus, at fringilla purus mauris a nunc.',
        'ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus.',
        'faucibus. Morbi vehicula. Pellentesque tincidunt tempus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (23, 6, 3, 'Jogász', 'Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin',
        'sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a,',
        'accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (24, 6, 4, 'Értékesítési asszisztens', 'neque. Morbi quis urna. Nunc quis arcu vel quam dignissim',
        'et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet',
        'vehicula aliquet libero. Integer in');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (25, 1, 5, 'Építőmérnök', 'Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor,',
        'eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta',
        'gravida sit amet, dapibus id, blandit at, nisi.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (26, 1, 1, 'Szoftverfejlesztő', 'lorem vitae odio sagittis semper. Nam tempor diam dictum sapien.',
        'iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque',
        'sit amet, consectetuer adipiscing elit. Aliquam');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (27, 2, 2, 'Szoftverfejlesztő', 'non nisi. Aenean eget metus. In nec orci. Donec nibh.',
        'Nunc commodo auctor velit. Aliquam nisl.', 'vehicula et, rutrum eu, ultrices sit amet,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (28, 2, 3, 'Építőmérnök', 'mi pede, nonummy ut, molestie in, tempus eu, ligula. Aenean',
        'ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est',
        'pharetra, felis eget varius ultrices, mauris ipsum porta elit,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (29, 3, 4, 'Értékesítési asszisztens', 'gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum',
        'risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas',
        'varius et, euismod et, commodo at, libero. Morbi accumsan');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (30, 3, 5, 'Humánerőforrás tanácsadó', 'egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie',
        'a, malesuada id, erat. Etiam vestibulum massa', 'consequat auctor, nunc nulla vulputate dui,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (31, 4, 1, 'Könyvelő', 'neque. Sed eget lacus. Mauris non dui nec urna suscipit',
        'orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas.',
        'consectetuer adipiscing elit. Aliquam auctor, velit');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (32, 4, 2, 'Értékesítési asszisztens', 'ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus.',
        'aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor',
        'arcu. Curabitur ut odio vel est');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (33, 5, 3, 'Értékesítési asszisztens', 'sit amet, dapibus id, blandit at, nisi. Cum sociis natoque',
        'enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero',
        'rutrum urna, nec luctus felis purus ac tellus. Suspendisse');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (34, 5, 4, 'Humánerőforrás tanácsadó', 'et risus. Quisque libero lacus, varius et, euismod et, commodo',
        'quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per',
        'non quam. Pellentesque habitant morbi tristique senectus et netus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (35, 6, 5, 'Építőmérnök', 'Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus,',
        'vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque',
        'rhoncus id, mollis nec, cursus a, enim. Suspendisse');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (36, 6, 1, 'Könyvelő', 'Nulla semper tellus id nunc interdum feugiat. Sed nec metus',
        'justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate,',
        'fringilla mi lacinia mattis. Integer eu lacus.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (37, 1, 2, 'Építőmérnök', 'Nulla eget metus eu erat semper rutrum. Fusce dolor quam,',
        'eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus',
        'vitae dolor. Donec fringilla. Donec');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (38, 1, 3, 'Könyvelő', 'dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris,',
        'eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class',
        'ornare, libero at auctor ullamcorper,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (39, 2, 4, 'Építőmérnök', 'dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse',
        'Aliquam rutrum lorem ac risus. Morbi metus. Vivamus',
        'metus. Vivamus euismod urna. Nullam lobortis quam a felis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (40, 2, 5, 'Jogász', 'congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum',
        'vel, convallis in, cursus et, eros. Proin ultrices. Duis', 'at augue id ante dictum cursus. Nunc');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (41, 3, 1, 'Tanár', 'metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper',
        'tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est',
        'nec tempus scelerisque, lorem ipsum sodales purus, in');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (42, 3, 2, 'Könyvelő', 'sit amet ante. Vivamus non lorem vitae odio sagittis semper.',
        'velit eget laoreet posuere, enim nisl', 'Curabitur egestas nunc sed libero. Proin sed turpis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (43, 4, 3, 'Könyvelő', 'ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida',
        'imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a, malesuada',
        'felis, adipiscing fringilla, porttitor vulputate, posuere');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (44, 4, 4, 'Könyvelő', 'diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget,',
        'et, eros. Proin ultrices. Duis volutpat nunc', 'nisi magna sed dui. Fusce aliquam, enim nec tempus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (45, 5, 5, 'Értékesítési asszisztens', 'Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor',
        'eu turpis. Nulla aliquet. Proin velit. Sed malesuada', 'a feugiat tellus lorem eu');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (46, 5, 1, 'Tanár', 'sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie',
        'in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing',
        'eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (47, 6, 2, 'Tudományos kutató', 'lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie.',
        'ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque',
        'dapibus gravida. Aliquam tincidunt, nunc ac mattis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (48, 6, 3, 'Tanár', 'Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi',
        'Proin nisl sem, consequat nec,', 'neque. In ornare sagittis felis. Donec');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (49, 1, 4, 'Tanár', 'ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus',
        'massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero.',
        'eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (50, 1, 5, 'Tanár', 'Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna.',
        'interdum enim non nisi. Aenean eget', 'elit. Etiam laoreet, libero et tristique pellentesque, tellus sem');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (51, 2, 1, 'Értékesítési asszisztens', 'augue malesuada malesuada. Integer id magna et ipsum cursus vestibulum.',
        'ornare, elit elit fermentum risus, at fringilla purus',
        'per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (52, 2, 2, 'Építőmérnök', 'Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna.',
        'lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa.',
        'sit amet, risus. Donec nibh enim, gravida sit amet, dapibus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (53, 3, 3, 'Tanár', 'Praesent eu nulla at sem molestie sodales. Mauris blandit enim',
        'sit amet nulla. Donec non justo. Proin non massa non ante',
        'nunc id enim. Curabitur massa. Vestibulum accumsan neque et');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (54, 3, 4, 'Tudományos kutató', 'vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy',
        'nulla. In tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis',
        'id enim. Curabitur massa. Vestibulum accumsan neque');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (55, 4, 5, 'Tudományos kutató', 'eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum',
        'cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu',
        'vehicula et, rutrum eu, ultrices sit amet, risus. Donec');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (56, 4, 1, 'Építőmérnök', 'magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna',
        'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede',
        'fringilla est. Mauris eu turpis. Nulla');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (57, 5, 2, 'Tanár', 'porttitor tellus non magna. Nam ligula elit, pretium et, rutrum',
        'dui, in sodales elit erat vitae risus. Duis a mi', 'feugiat nec, diam. Duis mi');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (58, 5, 3, 'Építőmérnök', 'Nunc ut erat. Sed nunc est, mollis non, cursus non,',
        'dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor.',
        'ante ipsum primis in faucibus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (59, 6, 4, 'Tanár', 'at fringilla purus mauris a nunc. In at pede. Cras',
        'ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus.',
        'quam. Pellentesque habitant morbi tristique');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (60, 6, 5, 'Tanár', 'et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,',
        'est ac mattis semper, dui lectus', 'mi lacinia mattis. Integer eu lacus. Quisque');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (61, 1, 1, 'Tanár', 'Donec tempor, est ac mattis semper, dui lectus rutrum urna,',
        'sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac',
        'egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (62, 1, 2, 'Értékesítési asszisztens', 'enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula',
        'enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus',
        'sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (63, 2, 3, 'Tanár', 'Nunc ut erat. Sed nunc est, mollis non, cursus non,',
        'lobortis quam a felis ullamcorper viverra. Maecenas iaculis', 'augue. Sed molestie. Sed id risus quis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (64, 2, 4, 'Humánerőforrás tanácsadó',
        'consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.',
        'enim nisl elementum purus, accumsan', 'amet, consectetuer adipiscing elit. Etiam laoreet, libero');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (65, 3, 5, 'Tudományos kutató', 'dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec,',
        'arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam',
        'imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (66, 3, 1, 'Jogász', 'cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet',
        'ut odio vel est tempor bibendum. Donec felis orci, adipiscing non,', 'leo, in lobortis tellus justo sit amet');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (67, 4, 2, 'Tudományos kutató', 'dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel,',
        'ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci',
        'rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (68, 4, 3, 'Tudományos kutató', 'urna, nec luctus felis purus ac tellus. Suspendisse sed dolor.',
        'augue malesuada malesuada. Integer id magna et', 'elit fermentum risus, at fringilla purus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (69, 5, 4, 'Értékesítési asszisztens', 'feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc,',
        'adipiscing, enim mi tempor lorem,', 'augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (70, 5, 5, 'Jogász', 'feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam',
        'nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque,',
        'imperdiet dictum magna. Ut tincidunt orci quis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (71, 6, 1, 'Tudományos kutató', 'sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis',
        'arcu iaculis enim, sit amet ornare lectus', 'tristique ac, eleifend vitae, erat. Vivamus nisi.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (72, 6, 2, 'Könyvelő', 'euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet,',
        'et, eros. Proin ultrices. Duis volutpat nunc sit amet metus.', 'eget, venenatis a, magna. Lorem ipsum dolor');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (73, 1, 3, 'Könyvelő', 'eros non enim commodo hendrerit. Donec porttitor tellus non magna.',
        'velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus.',
        'elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (74, 1, 4, 'Szoftverfejlesztő', 'Ut semper pretium neque. Morbi quis urna. Nunc quis arcu',
        'nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut',
        'faucibus orci luctus et ultrices posuere cubilia Curae Donec tincidunt.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (75, 2, 5, 'Építőmérnök', 'at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum',
        'semper egestas, urna justo faucibus lectus,', 'arcu. Sed et libero. Proin mi. Aliquam gravida');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (76, 2, 1, 'Tudományos kutató', 'euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate',
        'nulla. Integer vulputate, risus a ultricies adipiscing, enim',
        'Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (77, 3, 2, 'Értékesítési asszisztens', 'tincidunt. Donec vitae erat vel pede blandit congue. In scelerisque',
        'et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a',
        'pharetra. Nam ac nulla. In tincidunt congue turpis.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (78, 3, 3, 'Tudományos kutató', 'eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in',
        'est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia', 'nonummy ut, molestie in, tempus eu,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (79, 4, 4, 'Tudományos kutató', 'montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque',
        'mi lorem, vehicula et, rutrum eu, ultrices sit', 'mi eleifend egestas. Sed pharetra, felis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (80, 4, 5, 'Könyvelő', 'metus sit amet ante. Vivamus non lorem vitae odio sagittis', 'Nunc ut erat. Sed nunc',
        'Nam consequat dolor vitae dolor. Donec fringilla. Donec');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (81, 5, 1, 'Jogász', 'parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor.',
        'et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus.',
        'penatibus et magnis dis parturient montes,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (82, 5, 2, 'Könyvelő', 'quam. Pellentesque habitant morbi tristique senectus et netus et malesuada',
        'tortor. Nunc commodo auctor velit. Aliquam', 'Phasellus fermentum convallis ligula. Donec luctus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (83, 6, 3, 'Tudományos kutató', 'Ut tincidunt vehicula risus. Nulla eget metus eu erat semper',
        'blandit at, nisi. Cum sociis natoque penatibus et magnis dis', 'penatibus et magnis dis parturient montes,');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (84, 6, 4, 'Könyvelő', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices',
        'et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus.',
        'dolor. Donec fringilla. Donec feugiat metus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (85, 1, 5, 'Tudományos kutató', 'morbi tristique senectus et netus et malesuada fames ac turpis',
        'ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales',
        'nulla at sem molestie sodales. Mauris');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (86, 1, 1, 'Értékesítési asszisztens', 'molestie in, tempus eu, ligula. Aenean euismod mauris eu elit.',
        'metus sit amet ante. Vivamus non lorem', 'et netus et malesuada fames ac turpis egestas.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (87, 2, 2, 'Könyvelő', 'aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in',
        'tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id',
        'hendrerit a, arcu. Sed et libero. Proin mi. Aliquam');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (88, 2, 3, 'Szoftverfejlesztő',
        'consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.',
        'Nullam velit dui, semper et, lacinia vitae, sodales',
        'lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (89, 3, 4, 'Jogász', 'enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare',
        'tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor.',
        'justo. Praesent luctus. Curabitur egestas nunc sed');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (90, 3, 5, 'Könyvelő', 'posuere vulputate, lacus. Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse',
        'lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis',
        'ante. Nunc mauris sapien, cursus in, hendrerit');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (91, 4, 1, 'Értékesítési asszisztens', 'semper erat, in consectetuer ipsum nunc id enim. Curabitur massa.',
        'erat. Etiam vestibulum massa rutrum magna. Cras convallis',
        'amet, faucibus ut, nulla. Cras eu tellus eu augue');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (92, 4, 2, 'Könyvelő', 'turpis non enim. Mauris quis turpis vitae purus gravida sagittis.',
        'libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non,',
        'lacus. Mauris non dui nec urna suscipit nonummy. Fusce');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (93, 5, 3, 'Tudományos kutató', 'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin',
        'facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet',
        'tincidunt dui augue eu tellus. Phasellus');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (94, 5, 4, 'Szoftverfejlesztő', 'egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere',
        'odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit',
        'arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (95, 6, 5, 'Tudományos kutató', 'Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi,',
        'sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean',
        'orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (96, 6, 1, 'Könyvelő', 'non lorem vitae odio sagittis semper. Nam tempor diam dictum',
        'sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec',
        'ac turpis egestas. Fusce aliquet magna');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (97, 1, 2, 'Könyvelő', 'auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi',
        'nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet',
        'vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (98, 1, 3, 'Tudományos kutató', 'vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras',
        'porttitor tellus non magna. Nam ligula elit,',
        'Pellentesque habitant morbi tristique senectus et netus et malesuada');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (99, 2, 4, 'Könyvelő', 'arcu ac orci. Ut semper pretium neque. Morbi quis urna.',
        'ultrices a, auctor non, feugiat nec, diam.', 'semper tellus id nunc interdum feugiat.');
INSERT INTO POZICIO (pozicio_id, munka_tipus_id, ceg_id, feladatkor, leiras, elvaras, kinalat)
VALUES (100, 2, 5, 'Könyvelő', 'mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet,',
        'at augue id ante dictum cursus. Nunc mauris elit, dictum eu,', 'facilisi. Sed neque. Sed eget lacus.');


-- ÉRTÉKELÉSEK FELTÖLTÉSE
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (1, 1, 2, 8, 'blandit mattis. Cras eget nisi dictum augue malesuada malesuada. Integer');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (2, 3, 2, 8, 'erat, in consectetuer ipsum nunc id enim. Curabitur massa.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (3, 2, 3, 8, 'Fusce');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (4, 4, 4, 9, 'eros.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (5, 4, 3, 8, 'cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (6, 2, 3, 3, 'eget mollis lectus pede et');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (7, 2, 4, 7, 'cursus in, hendrerit consectetuer, cursus et, magna. Praesent');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (8, 3, 2, 3, 'ad litora torquent per conubia');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (9, 3, 2, 5, 'justo sit amet nulla.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (10, 3, 3, 9, 'nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (11, 3, 3, 5, 'ligula elit, pretium et, rutrum non, hendrerit id, ante.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (12, 4, 5, 2, 'interdum libero dui');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (13, 2, 5, 9, 'a feugiat tellus lorem eu metus. In lorem. Donec');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (14, 3, 2, 7, 'ligula tortor, dictum eu, placerat eget, venenatis a,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (15, 4, 1, 8, 'Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (16, 2, 5, 5, 'tempus,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (17, 3, 2, 4, 'sem, vitae aliquam eros turpis non enim. Mauris');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (18, 5, 2, 2, 'luctus vulputate, nisi sem semper erat,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (19, 3, 3, 7, 'dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (20, 4, 3, 7, 'habitant morbi tristique senectus et netus et malesuada fames ac');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (21, 4, 2, 5, 'lectus rutrum urna, nec luctus felis purus ac');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (22, 4, 2, 8, 'orci sem eget massa.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (23, 1, 4, 7, 'tincidunt orci quis lectus. Nullam suscipit, est ac facilisis');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (24, 3, 2, 2, 'nulla.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (25, 3, 5, 7, 'Aliquam tincidunt,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (26, 2, 2, 10, 'Suspendisse tristique neque venenatis');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (27, 1, 2, 9, 'at fringilla');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (28, 2, 4, 10, 'enim. Mauris quis');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (29, 2, 4, 6, 'id, erat. Etiam vestibulum massa rutrum magna. Cras convallis');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (30, 3, 2, 10, 'eros turpis non enim. Mauris quis turpis');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (31, 2, 2, 4, 'libero est, congue');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (32, 5, 1, 5, 'leo, in lobortis tellus justo sit amet nulla. Donec');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (33, 1, 2, 4, 'venenatis lacus. Etiam');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (34, 3, 4, 1, 'placerat eget, venenatis a, magna. Lorem ipsum dolor sit');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (35, 3, 2, 10, 'ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (36, 2, 5, 7, 'Suspendisse commodo tincidunt nibh.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (37, 4, 1, 1, 'tellus. Phasellus elit pede, malesuada');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (38, 2, 3, 8, 'arcu. Morbi sit amet');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (39, 2, 3, 4, 'luctus et ultrices posuere cubilia Curae Phasellus ornare. Fusce mollis.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (40, 4, 2, 4, 'sit amet ante. Vivamus non lorem');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (41, 2, 4, 8, 'vulputate velit eu sem. Pellentesque ut ipsum');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (42, 4, 5, 9, 'senectus et netus et malesuada fames ac');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (43, 2, 2, 7, 'et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (44, 4, 1, 5, 'mauris ut mi. Duis risus odio, auctor vitae,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (45, 1, 2, 2, 'Nulla facilisi. Sed neque. Sed eget lacus.');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (46, 3, 4, 3, 'mattis ornare,');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (47, 1, 2, 6, 'amet luctus');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (48, 4, 2, 1, 'ligula. Donec luctus aliquet odio. Etiam ligula');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (49, 4, 3, 1, 'luctus et ultrices posuere cubilia Curae');
INSERT INTO ERTEKELES (ertekeles_id, felhasznalo_id, ceg_id, pontszam, szoveg)
VALUES (50, 3, 4, 9, 'amet diam eu dolor egestas rhoncus. Proin nisl');


-- JELENTKEZÉSEK FELTÖLTÉSE
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (1, 1, 1, 'szeretnek jelentkezni', null);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (2, 1, 2, 'jó helyen vagyok?', null);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (3, 2, 1, 'fel tudnátok venni', null);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (4, 3, 1, 'nagyon szeretném ezt a lehetőséget', null);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (5, 4, 1, 'én már csináltam ilyet', null);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (6, 2, 13, 'quam vel', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (7, 3, 18, 'Nulla semper', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (8, 2, 10, 'non, lacinia', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (9, 4, 10, 'ac orci.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (10, 3, 10, 'Nunc laoreet', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (11, 2, 7, 'pretium et,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (12, 2, 14, 'ridiculus mus.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (13, 5, 9, 'senectus et', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (14, 2, 11, 'pellentesque, tellus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (15, 2, 7, 'mattis. Cras', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (16, 2, 15, 'quam. Pellentesque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (17, 4, 18, 'lobortis quis,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (18, 2, 7, 'odio sagittis', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (19, 3, 10, 'malesuada. Integer', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (20, 4, 19, 'mi tempor', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (21, 4, 2, 'libero. Proin', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (22, 2, 19, 'tincidunt congue', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (23, 4, 18, 'diam. Sed', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (24, 3, 15, 'lacus, varius', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (25, 4, 7, 'cursus luctus,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (26, 3, 18, 'cursus non,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (27, 4, 8, 'sem ut', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (28, 3, 6, 'eget metus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (29, 1, 14, 'libero. Integer', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (30, 4, 18, 'luctus. Curabitur', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (31, 3, 4, 'risus quis', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (32, 4, 11, 'erat. Vivamus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (33, 2, 9, 'iaculis enim,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (34, 5, 3, 'ac, feugiat', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (35, 1, 2, 'a, dui.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (36, 3, 15, 'tristique ac,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (37, 4, 8, 'at, egestas', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (38, 2, 18, 'tellus non', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (39, 3, 10, 'convallis convallis', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (40, 1, 8, 'Curabitur consequat,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (41, 3, 11, 'sit amet', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (42, 5, 2, 'ornare, elit', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (43, 3, 8, 'non massa', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (44, 5, 19, 'tellus. Aenean', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (45, 5, 5, 'mollis lectus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (46, 3, 1, 'senectus et', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (47, 4, 14, 'posuere, enim', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (48, 1, 18, 'Aenean gravida', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (49, 5, 18, 'penatibus et', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (50, 1, 6, 'laoreet lectus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (51, 3, 17, 'dignissim magna', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (52, 5, 8, 'ultrices, mauris', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (53, 3, 9, 'taciti sociosqu', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (54, 4, 19, 'Curae Phasellus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (55, 1, 7, 'tristique senectus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (56, 4, 19, 'a nunc.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (57, 5, 13, 'Nulla tincidunt,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (58, 4, 11, 'enim diam', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (59, 3, 1, 'urna. Nunc', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (60, 3, 19, 'facilisis. Suspendisse', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (61, 3, 15, 'Sed eu', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (62, 4, 1, 'auctor ullamcorper,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (63, 3, 6, 'hendrerit a,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (64, 2, 16, 'penatibus et', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (65, 4, 18, 'neque. Sed', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (66, 3, 9, 'urna. Nunc', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (67, 3, 9, 'aliquet. Proin', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (68, 3, 14, 'sed pede', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (69, 1, 20, 'Nunc mauris.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (70, 4, 8, 'sem egestas', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (71, 4, 9, 'diam vel', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (72, 2, 12, 'enim diam', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (73, 3, 13, 'odio. Etiam', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (74, 4, 2, 'enim, condimentum', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (75, 3, 2, 'odio. Nam', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (76, 4, 8, 'ipsum. Curabitur', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (77, 5, 15, 'eget metus.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (78, 5, 3, 'ligula eu', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (79, 2, 7, 'facilisis lorem', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (80, 5, 13, 'In condimentum.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (81, 3, 13, 'libero. Proin', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (82, 2, 6, 'ornare, lectus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (83, 2, 14, 'orci, consectetuer', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (84, 4, 10, 'Vivamus molestie', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (85, 2, 14, 'urna. Vivamus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (86, 1, 4, 'fringilla mi', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (87, 3, 15, 'sit amet', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (88, 4, 5, 'tincidunt dui', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (89, 2, 14, 'ipsum sodales', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (90, 1, 17, 'lectus, a', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (91, 3, 10, 'convallis erat,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (92, 4, 12, 'erat vel', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (93, 3, 13, 'orci luctus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (94, 3, 3, 'rhoncus. Proin', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (95, 1, 14, 'vehicula aliquet', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (96, 3, 18, 'Pellentesque tincidunt', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (97, 4, 15, 'Duis elementum,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (98, 4, 5, 'nascetur ridiculus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (99, 3, 3, 'Mauris vestibulum,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (100, 1, 11, 'amet nulla.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (101, 5, 8, 'interdum feugiat.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (102, 5, 9, 'sapien, cursus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (103, 4, 16, 'congue, elit', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (104, 3, 2, 'sagittis semper.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (105, 1, 5, 'Duis volutpat', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (106, 3, 18, 'Vivamus euismod', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (107, 2, 14, 'elit. Nulla', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (108, 4, 18, 'Donec non', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (109, 5, 19, 'amet orci.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (110, 3, 3, 'Ut tincidunt', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (111, 5, 5, 'ipsum non', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (112, 3, 8, 'tincidunt dui', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (113, 3, 15, 'cursus in,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (114, 4, 9, 'placerat, augue.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (115, 2, 4, 'adipiscing lobortis', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (116, 5, 19, 'et ipsum', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (117, 5, 9, 'et, rutrum', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (118, 2, 9, 'vestibulum lorem,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (119, 2, 3, 'orci quis', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (120, 3, 13, 'nisi. Cum', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (121, 3, 13, 'Ut tincidunt', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (122, 3, 3, 'a purus.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (123, 4, 20, 'risus. In', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (124, 2, 5, 'porta elit,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (125, 2, 12, 'nulla. Cras', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (126, 3, 1, 'ipsum. Phasellus', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (127, 4, 5, 'tortor. Integer', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (128, 3, 13, 'purus. Maecenas', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (129, 1, 8, 'ut, nulla.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (130, 1, 2, 'Suspendisse aliquet', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (131, 4, 5, 'et ultrices', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (132, 3, 14, 'primis in', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (133, 3, 8, 'leo. Vivamus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (134, 3, 5, 'enim. Curabitur', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (135, 1, 11, 'mi felis,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (136, 1, 2, 'scelerisque scelerisque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (137, 4, 6, 'Phasellus nulla.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (138, 3, 13, 'Donec egestas.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (139, 5, 6, 'Curabitur sed', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (140, 5, 15, 'lectus quis', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (141, 3, 12, 'purus. Maecenas', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (142, 5, 14, 'neque. Nullam', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (143, 4, 4, 'Etiam gravida', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (144, 5, 16, 'interdum enim', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (145, 3, 12, 'erat, eget', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (146, 3, 10, 'lectus. Cum', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (147, 5, 4, 'in sodales', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (148, 4, 6, 'Phasellus vitae', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (149, 4, 1, 'enim non', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (150, 4, 3, 'auctor velit.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (151, 3, 9, 'magna. Ut', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (152, 4, 12, 'dolor vitae', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (153, 4, 8, 'ornare tortor', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (154, 4, 18, 'tellus, imperdiet', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (155, 2, 11, 'ullamcorper eu,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (156, 2, 2, 'vel sapien', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (157, 3, 1, 'lectus pede,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (158, 4, 14, 'dapibus id,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (159, 2, 15, 'Suspendisse sagittis.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (160, 4, 7, 'non, sollicitudin', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (161, 2, 3, 'Sed malesuada', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (162, 1, 5, 'suscipit nonummy.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (163, 3, 4, 'turpis vitae', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (164, 4, 5, 'consectetuer, cursus', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (165, 4, 14, 'Nullam scelerisque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (166, 1, 13, 'augue malesuada', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (167, 4, 3, 'Phasellus dolor', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (168, 4, 10, 'ad litora', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (169, 3, 12, 'amet, consectetuer', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (170, 2, 16, 'nec, eleifend', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (171, 5, 10, 'eu, ligula.', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (172, 4, 3, 'Nam tempor', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (173, 3, 9, 'libero. Morbi', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (174, 1, 13, 'ultricies ornare,', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (175, 2, 15, 'risus. Quisque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (176, 1, 14, 'cursus in,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (177, 3, 1, 'In tincidunt', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (178, 2, 14, 'sagittis semper.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (179, 3, 14, 'egestas. Fusce', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (180, 3, 16, 'nisi magna', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (181, 4, 10, 'diam vel', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (182, 3, 14, 'elit, pellentesque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (183, 4, 7, 'orci sem', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (184, 2, 19, 'Mauris quis', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (185, 5, 2, 'Pellentesque ultricies', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (186, 4, 9, 'Nunc mauris.', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (187, 3, 15, 'neque sed', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (188, 3, 12, 'elementum purus,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (189, 4, 15, 'Etiam ligula', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (190, 3, 5, 'magna. Nam', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (191, 4, 9, 'metus. In', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (192, 5, 7, 'nulla ante,', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (193, 3, 14, 'velit. Quisque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (194, 4, 16, 'id, mollis', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (195, 4, 7, 'eu nulla', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (196, 4, 11, 'velit eu', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (197, 5, 4, 'mauris. Integer', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (198, 1, 6, 'tempus mauris', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (199, 5, 6, 'vehicula. Pellentesque', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (200, 2, 17, 'Morbi accumsan', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (201, 4, 18, 'vel arcu', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (202, 5, 18, 'Nunc ut', 1);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (203, 1, 18, 'lectus. Nullam', 0);
INSERT INTO JELENTKEZES (jelentkezes_id, felhasznalo_id, pozicio_id, uzenet, elutasitva)
VALUES (204, 5, 3, 'amet nulla.', 1);

-- Trigger a maximum jelentkezések számának limitálására
CREATE
OR REPLACE TRIGGER JEL_MAX_TRIGGER
BEFORE INSERT OR
UPDATE ON JELENTKEZES
    FOR EACH ROW
DECLARE
v_count NUMBER;
BEGIN
SELECT COUNT(*)
INTO v_count
FROM JELENTKEZES
WHERE FELHASZNALO_ID = :NEW.FELHASZNALO_ID;

IF
v_count >= 2 THEN
        RAISE_APPLICATION_ERROR(-20001, 'Csak két jelentkezésed lehet!');
END IF;
END;
